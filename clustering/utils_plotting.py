import os
import numpy as np
import matplotlib.pyplot as plt

plt.rcParams["font.family"] = ["serif"]
plt.rcParams["axes.grid"] = True
plt.rcParams["grid.alpha"] = 0.8
plt.rcParams["axes.axisbelow"] = True
plt.rcParams["axes.linewidth"] = 1.0
plt.rcParams["axes.titleweight"] = "bold"
plt.rcParams["axes.titlecolor"] = "#4A4A4A"
plt.rcParams["patch.linewidth"] = 1.5
plt.rcParams["ytick.right"] = True

larger = 20  # 28
large = 18  # 26
med = 16  # 20

_params = {
    "axes.titlesize": larger,
    "legend.fontsize": med,
    "figure.figsize": (16, 10),
    "axes.labelsize": larger,
    "xtick.labelsize": large,
    "ytick.labelsize": large,
    "figure.titlesize": large,
    "xtick.bottom": True,
    "xtick.direction": "out",
    "ytick.direction": "out",
    "xtick.major.size": 10,
    "ytick.major.size": 10,
    "xtick.minor.size": 6,
    "ytick.minor.size": 6,
    "ytick.left": True,
}
plt.rcParams.update(_params)


def plot_dnn_nodes(prediction, weight, sig_processes, bg_processes, output_path):
    bins = np.linspace(0, 1, 11)  # 10 bins between 0 and 1
    pm_idx = np.argmax(prediction, axis=1)
    for dnn_node in range(prediction.shape[1]):

        bg_prediction = [
            prediction.max(axis=1)[np.all([proc_mask, pm_idx == dnn_node], axis=0)]
            for proc_mask in bg_processes.values()
        ]
        sig_prediction = [
            prediction.max(axis=1)[np.all([proc_mask, pm_idx == dnn_node], axis=0)]
            for proc_mask in sig_processes.values()
        ]
        bg_weight = [
            weight[np.all([proc_mask, pm_idx == dnn_node], axis=0)]
            for proc_mask in bg_processes.values()
        ]
        sig_weight = [
            weight[np.all([proc_mask, pm_idx == dnn_node], axis=0)]
            for proc_mask in sig_processes.values()
        ]

        colours = [
            (120 / 256, 33 / 256, 80 / 256),
            (55 / 256, 126 / 256, 184 / 256),
            (40 / 256, 53 / 256, 135 / 256),
            (0 / 256, 102 / 256, 102 / 256),
            (199 / 256, 21 / 256, 133 / 256),
            (244 / 256, 216 / 256, 44 / 256),
            (117 / 256, 171 / 256, 79 / 256),
        ]
        plt.figure(figsize=(10, 8))  # figsize=(10, 8), dpi=500 für gute Auflösung
        counts_bg, bins, _ = plt.hist(
            bg_prediction,
            bins=bins,
            weights=bg_weight,
            stacked=True,
            label=list(bg_processes.keys()),
            color=colours,
            log=True,
        )
        counts_sig, bins_sig, _ = plt.hist(
            sig_prediction,
            bins=bins,
            weights=sig_weight,
            histtype="step",
            stacked=False,
            linewidth=2,
            label="VH",
            color=(244 / 256, 67 / 256, 54 / 256),
            log=True,
        )

        plt.ylim(top=50 * max(counts_bg.sum(axis=0)))
        plt.ylabel("Entries")
        plt.xlabel("DNN Score")
        plt.legend(
            loc="upper left",
            bbox_to_anchor=(0.0, 0.7, 1, 0.3),
            ncol=5,
            mode="expand",
        )
        plt.savefig(os.path.join(output_path, f"DNN-node_{dnn_node}.png"))
        plt.close()
