import argparse
import json
import os
import numpy as np
import matplotlib.pyplot as plt
import mplhep
from pathlib import Path
from itertools import chain

plt.rcParams["font.family"] = ["serif"]
plt.rcParams["axes.grid"] = True
plt.rcParams["grid.alpha"] = 0.8
plt.rcParams["axes.axisbelow"] = True
plt.rcParams["axes.linewidth"] = 1.0
plt.rcParams["axes.titleweight"] = "bold"
plt.rcParams["axes.titlesize"] = "x-large"
plt.rcParams["axes.labelsize"] = "large"
plt.rcParams["axes.titlecolor"] = "#4A4A4A"
plt.rcParams["patch.linewidth"] = 1.5
plt.rcParams["ytick.right"] = True

larger = 28
large = 26
med = 20

_params = {
    "axes.titlesize": larger,
    "legend.fontsize": med,
    "figure.figsize": (16, 10),
    "axes.labelsize": larger,
    "xtick.labelsize": large,
    "ytick.labelsize": large,
    "figure.titlesize": large,
    "xtick.bottom": True,
    "xtick.direction": "in",
    "ytick.direction": "in",
    "xtick.major.size": 12,
    "ytick.major.size": 12,
    "xtick.minor.size": 8,
    "ytick.minor.size": 8,
    "ytick.left": True,
    }
plt.rcParams.update(_params)

# limit = [3.670840836070664,4.945850444956205,6.907365570644292,9.695125160407345,13.136993084011172]
limits = {
        "base": [ 1.395599365234375,1.8544716835021973,2.5703125,3.5641138553619385,4.743332386016846 ],
        150: [3.43505859375,4.546852111816406,6.28125,8.734908103942871, 11.609702110290527],
        250: [3.020263671875,4.013324737548828,5.5625,7.713219165802002, 10.265206336975098],
        500: [2.273681640625,3.0212669372558594,4.1875,5.806580543518066, 7.727739334106445],
        }


parser = argparse.ArgumentParser(description='Clustering algorithm')
parser.add_argument('output_path', metavar='o', type=str,
                    help='output path of Clustering')

args = parser.parse_args()

benchmarks = []

fig, ax = plt.subplots(1, 1, figsize=(15, 10))
x_min = 0
x_max = np.max([k for k in limits.keys() if isinstance(k, int) ]) * 1.1
limit_x = [x_min, x_max]
mplhep.cms.label(label="Private Work", loc=0, fontsize=large)

ax.fill_between(limit_x, limits["base"][3], limits["base"][4], alpha=0.2, color="yellow")
ax.fill_between(limit_x, limits["base"][1], limits["base"][3], alpha=0.2, color="green")
ax.fill_between(limit_x, limits["base"][1], limits["base"][0], alpha=0.2, color="yellow")
ax.axhline(y=limits["base"][2], color="black", linestyle="dashdot", label="Standard binning")

x_values = [k for k in limits.keys() if isinstance(k, int)] 
y_values = np.array([limits[x][2] for x in x_values])
y_up = np.array([limits[x][3] for x in x_values])
y_up_up = np.array([limits[x][4] for x in x_values])
y_down = np.array([limits[x][1] for x in x_values])
y_down_down = np.array([limits[x][0] for x in x_values])

ax.errorbar(x_values, y_values, yerr=[y_values-y_down, y_up-y_values], marker="o", ls='none', label="Clustering", color="black", capsize=2.)
ax.errorbar(x_values, y_values, yerr=[y_values-y_down_down, y_up_up-y_values], marker=None, ls='none', color="black", lw=1.)

# ax.errorbar( [d["n_clusters"] for d in benchmarks], [d["exp_limit"][2] for d in benchmarks], yerr=[[d["exp_limit"][2] - d["exp_limit"][1] for d in benchmarks], [d["exp_limit"][3] - d["exp_limit"][2] for d in benchmarks]], marker="o", ls='none', label="Clustering", color="black", capsize=2.)
# ax.errorbar( [d["n_clusters"] for d in benchmarks], [d["exp_limit"][2] for d in benchmarks], yerr=[[d["exp_limit"][2] - d["exp_limit"][0] for d in benchmarks], [d["exp_limit"][4] - d["exp_limit"][2] for d in benchmarks]], marker=None, ls='none', color="black", lw=1.)

ax.set_xlim(x_min, x_max)
ax.set_ylim(0.5, 10.)
ax.set_xlabel("# clusters", fontsize=larger)
ax.set_ylabel("Limit", fontsize=larger)
ax.legend()
fig.savefig(os.path.join(args.output_path, "benchmark.png"))

