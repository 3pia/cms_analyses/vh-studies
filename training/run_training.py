import argparse

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("--debug", type=bool, action="store_true", help="True = explorative Daten")
    parser.add_argument("--nsplits", "-n", type=int, help="Number of test splits") 
    parser.add_argument("--split", "-s", type=int, help="Index of test-set split") 
    parser.add_argument("--channel", "-c", type=str, default="inclusive", help="Channel") #1el, 2el, 1mu, 2mu, all_channels, 1lep, 2lep, el, mu
    parser.add_argument("--inputdir", "-i", type=str, help="Input directory")
    parser.add_argument("--epochs", "-e", type=int, help="anzahl epochen")
    args = parser.parse_args()

    data, test_data = load_data(args.inputdir, args.nsplits, args.split, args.channel)

def load_data(inputs, n_splits, split_index, channel):
    data = None
    test_data = None
    return data, test_data

if __name__ == "__main__":
    main()
