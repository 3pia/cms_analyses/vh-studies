#!/bin/bash
# inpdir=/net/scratch/cms/dihiggs/store/vh/Run2_pp_13TeV_2017/CoffeaProcessor/run_01/vh/selection/LogitExporterProcessor/
#directory=output/benchmark_full_06
#inpdir=/net/scratch/cms/dihiggs/store/vh/Run2_pp_13TeV_2017/CoffeaProcessor/test_merge_02/vh/selection/LogitExporterProcessor/
directory=/net/scratch/cms/dihiggs/store/vh/vh-studies/clustering/output/UL_prod01_softmax
inpdir=/net/scratch/cms/dihiggs/store/vh/Run2_pp_13TeV_UL_2017/CoffeaProcessor/prod01/vh/selection/LogitExporterProcessor/
maxiter=400
MEMORY=15000
#categories="ee_boosted_1b mumu_boosted_1b e_boosted_1b mu_boosted_1b ee_resolved_1b ee_resolved_2b mumu_resolved_1b mumu_resolved_2b e_resolved_2b e_resolved_1b mu_resolved_2b mu_resolved_1b"
categories="incl"
submit -m $MEMORY -L $directory/logs python3 run_clustering.py $inpdir $directory --n-clusters 10 --categories $categories --verbose 1 --max-iter $maxiter --mini-batch --use-softmax
submit -m $MEMORY -L $directory/logs python3 run_clustering.py $inpdir $directory --n-clusters 25 --categories $categories --verbose 1 --max-iter $maxiter --mini-batch --use-softmax
submit -m $MEMORY -L $directory/logs python3 run_clustering.py $inpdir $directory --n-clusters 50 --categories $categories --verbose 1 --max-iter $maxiter --mini-batch --use-softmax
#submit -m $MEMORY -L $directory/logs python3 run_clustering.py $inpdir $directory --n-clusters 75 --categories $categories --verbose 1 --max-iter $maxiter --mini-batch --use-softmax
submit -m $MEMORY -L $directory/logs python3 run_clustering.py $inpdir $directory --n-clusters 100 --categories $categories --verbose 1 --max-iter $maxiter --mini-batch --use-softmax
submit -m $MEMORY -L $directory/logs python3 run_clustering.py $inpdir $directory --n-clusters 150 --categories $categories --verbose 1 --max-iter $maxiter --mini-batch --use-softmax
submit -m $MEMORY -L $directory/logs python3 run_clustering.py $inpdir $directory --n-clusters 200 --categories $categories --verbose 1 --max-iter $maxiter --mini-batch --use-softmax
submit -m $MEMORY -L $directory/logs python3 run_clustering.py $inpdir $directory --n-clusters 250 --categories $categories --verbose 1 --max-iter $maxiter --mini-batch --use-softmax
submit -m $MEMORY -L $directory/logs python3 run_clustering.py $inpdir $directory --n-clusters 325 --categories $categories --verbose 1 --max-iter $maxiter --mini-batch --use-softmax
submit -m $MEMORY -L $directory/logs python3 run_clustering.py $inpdir $directory --n-clusters 500 --categories $categories --verbose 1 --max-iter $maxiter --mini-batch --use-softmax
submit -m $MEMORY -L $directory/logs python3 run_clustering.py $inpdir $directory --n-clusters 750 --categories $categories --verbose 1 --max-iter $maxiter --mini-batch --use-softmax
