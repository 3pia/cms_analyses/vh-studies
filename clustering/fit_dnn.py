import argparse
import pyhf
import os
import numpy as np
import time
import json
import logging

from utils_fitting import calculate_limits
from utils_plotting import plot_dnn_nodes

log = logging.getLogger(__name__)


def split_processes(procid):
    log.info("Splitting processes into signal and background")
    sig_processes = {
        "VH": np.any(
            [procid == 9421, procid == 9502, procid == 9411, procid == 9504], axis=0
        ).flatten(),
    }

    bg_processes = {
        "ttVH": np.all([procid >= 9900, procid < 9950], axis=0).flatten(),
        "ttH": np.all([procid >= 9600, procid < 9650], axis=0).flatten(),
        "ttVV": np.all([procid >= 7300, procid < 7350], axis=0).flatten(),
        "ttV": np.all([procid >= 7000, procid < 7250], axis=0).flatten(),
        "VBFH": np.all([procid >= 9200, procid < 9250], axis=0).flatten(),
        "ggH": np.all([procid >= 9100, procid < 9150], axis=0).flatten(),
        "vvv": np.all([procid >= 4600, procid < 4650], axis=0).flatten(),
        "vv": np.all([procid >= 4000, procid < 4550], axis=0).flatten(),
        "dy": np.all([procid >= 3000, procid < 3500], axis=0).flatten(),
        "QCD": np.all([procid >= 8000, procid < 9000], axis=0).flatten(),
        "wjets": np.all([procid >= 6000, procid < 7000], axis=0).flatten(),
        "st": np.all([procid >= 5000, procid < 6000], axis=0).flatten(),
        "tt": np.all([procid >= 2000, procid < 3000], axis=0).flatten(),
    }
    return sig_processes, bg_processes


def split_processes_plotting(procid):
    log.info("Splitting processes into signal and background for plotting")
    sig_processes = {
        "VH": np.any(
            [procid == 9421, procid == 9502, procid == 9411, procid == 9504], axis=0
        ).flatten(),
    }

    ttV_mask = np.all([procid >= 7000, procid < 8000], axis=0)
    rare_mask = np.all([procid >= 20000, procid < 21000], axis=0)
    ggH_VBF_mask = np.all([procid >= 9000, procid < 9300], axis=0)
    ttH_mask = np.all([procid >= 9600, procid < 9950], axis=0)

    bg_processes = {
        "Other": np.any([ttV_mask, rare_mask, ggH_VBF_mask, ttH_mask], axis=0).flatten(),
        "VV(V)": np.all([procid >= 4000, procid < 5000], axis=0).flatten(),
        "DY": np.all([procid >= 3000, procid < 3500], axis=0).flatten(),
        "QCD": np.all([procid >= 8000, procid < 9000], axis=0).flatten(),
        "W + Jets": np.all([procid >= 6000, procid < 7000], axis=0).flatten(),
        "ST": np.all([procid >= 5000, procid < 6000], axis=0).flatten(),
        "TT": np.all([procid >= 2000, procid < 3000], axis=0).flatten(),
    }
    return sig_processes, bg_processes


def obtain_binning(prediction, weight, sig_processes, bg_processes):
    bins = np.linspace(0, 1, 11)  # 10 bins between 0 and 1
    pm_idx = np.argmax(prediction, axis=1)
    for dnn_node in range(prediction.shape[1]):

        bg_prediction = [
            prediction.max(axis=1)[np.all([proc_mask, pm_idx == dnn_node], axis=0)]
            for proc_mask in bg_processes.values()
        ]
        sig_prediction = [
            prediction.max(axis=1)[np.all([proc_mask, pm_idx == dnn_node], axis=0)]
            for proc_mask in sig_processes.values()
        ]
        bg_weight = [
            weight[np.all([proc_mask, pm_idx == dnn_node], axis=0)]
            for proc_mask in bg_processes.values()
        ]
        sig_weight = [
            weight[np.all([proc_mask, pm_idx == dnn_node], axis=0)]
            for proc_mask in sig_processes.values()
        ]

        # first bin prediction
        # digitize indices start at 1!!!
        bin_idx_bg = [
            np.digitize(bg_prediction[process], bins) for process in range(len(bg_prediction))
        ]
        bin_idx_sig = [
            np.digitize(sig_prediction[process], bins) for process in range(len(sig_prediction))
        ]

        # digitize indices start at 1!!!
        background_counts = np.array(
            [
                [max(np.sum(weight[idx == i]), 1e-5) for i in range(1, len(bins))]
                for weight, idx in zip(bg_weight, bin_idx_bg)
            ]
        )
        signal_counts = np.array(
            [
                [max(np.sum(weight[idx == i]), 1e-5) for i in range(1, len(bins))]
                for weight, idx in zip(sig_weight, bin_idx_sig)
            ]
        )
        # MC uncertainty
        background_uncs = np.array(
            [
                [max(np.sqrt(np.sum(weight[idx == i] ** 2)), 1e-5) for i in range(1, len(bins))]
                for weight, idx in zip(bg_weight, bin_idx_bg)
            ]
        )
        signal_uncs = np.array(
            [
                [max(np.sqrt(np.sum(weight[idx == i] ** 2)), 1e-5) for i in range(1, len(bins))]
                for weight, idx in zip(sig_weight, bin_idx_sig)
            ]
        )

        # concatenate all nodes for fit
        if dnn_node == 0:
            all_bg_counts = background_counts
            all_sig_counts = signal_counts
            all_bg_unc = background_uncs
            all_sig_unc = signal_uncs
        else:
            all_bg_counts = np.concatenate((all_bg_counts, background_counts), axis=1)
            all_sig_counts = np.concatenate((all_sig_counts, signal_counts), axis=1)
            all_bg_unc = np.concatenate((all_bg_unc, background_uncs), axis=1)
            all_sig_unc = np.concatenate((all_sig_unc, signal_uncs), axis=1)

    signals = {
        label: (data, uncs)
        for label, data, uncs in zip(sig_processes.keys(), all_sig_counts, all_sig_unc.tolist())
    }

    backgrounds = {
        label: (data, uncs)
        for label, data, uncs in zip(bg_processes.keys(), all_bg_counts, all_bg_unc.tolist())
    }
    return signals, backgrounds


def main(input_path, output_path, meta_input):
    # inputs
    prediction = np.load(os.path.join(input_path, "predictions.npy"))
    process_id = np.load(os.path.join(meta_input, "incl+procid.npy"))
    event_weight = np.load(os.path.join(meta_input, "incl+weight.npy"))

    # make plots
    sig_processes_plotting, bg_processes_plotting = split_processes_plotting(process_id)
    plot_dnn_nodes(
        prediction,
        event_weight,
        sig_processes_plotting,
        bg_processes_plotting,
        output_path,
    )

    # fitting
    sig_processes, bg_processes = split_processes(process_id)
    signals, backgrounds = obtain_binning(prediction, event_weight, sig_processes, bg_processes)

    exp_limits = calculate_limits(signals, backgrounds)

    with open(os.path.join(output_path, "limits.json"), "w") as limits_file:
        out = {
            "exp_limit": list(map(float, exp_limits)),
        }
        json.dump(out, limits_file)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Fit comparision between multiple DNNs")
    parser.add_argument("input_path", metavar="i", type=str, help="input path of files")
    parser.add_argument("output_path", metavar="o", type=str, help="output path of Limits")
    parser.add_argument(
        "meta_input",
        type=str,
        help="input path of meta files (process id and event weight)",
    )

    args = parser.parse_args()

    FORMAT = "Fit: %(levelname)s %(message)s"
    logging.basicConfig(
        level=20,
        format=FORMAT,
    )

    main(args.input_path, args.output_path, args.meta_input)
