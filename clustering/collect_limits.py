import argparse
import json
import os
import numpy as np
import matplotlib.pyplot as plt
import mplhep
from pathlib import Path

plt.rcParams["font.family"] = ["serif"]
plt.rcParams["axes.grid"] = True
plt.rcParams["grid.alpha"] = 0.8
plt.rcParams["axes.axisbelow"] = True
plt.rcParams["axes.linewidth"] = 1.0
plt.rcParams["axes.titleweight"] = "bold"
plt.rcParams["axes.titlesize"] = "x-large"
plt.rcParams["axes.labelsize"] = "large"
plt.rcParams["axes.titlecolor"] = "#4A4A4A"
plt.rcParams["patch.linewidth"] = 1.5
plt.rcParams["ytick.right"] = True

larger = 28
large = 26
med = 20

_params = {
    "axes.titlesize": larger,
    "legend.fontsize": med,
    "figure.figsize": (16, 10),
    "axes.labelsize": larger,
    "xtick.labelsize": large,
    "ytick.labelsize": large,
    "figure.titlesize": large,
    "xtick.bottom": True,
    "xtick.direction": "in",
    "ytick.direction": "in",
    "xtick.major.size": 12,
    "ytick.major.size": 12,
    "xtick.minor.size": 8,
    "ytick.minor.size": 8,
    "ytick.left": True,
}
plt.rcParams.update(_params)

# limit = [3.670840836070664,4.945850444956205,6.907365570644292,9.695125160407345,13.136993084011172]
# limit = [1.3808582560407874,1.6018493990224922,2.0061910810828296,2.623466996047224,3.4018910827700055]
limit = [
    1.4206889144013857,
    1.6920425662380856,
    2.1346753600357258,
    2.809916063581696,
    3.6878692311541763,
]
norm = limit[2]

parser = argparse.ArgumentParser(description="Clustering algorithm")
parser.add_argument(
    "input_path", metavar="i", type=str, help="input path of LogitExporterProcessor"
)
parser.add_argument("output_path", metavar="o", type=str, help="output path of Clustering")

args = parser.parse_args()

benchmarks = []

for inp_file in Path(args.input_path).glob("*limits.json"):
    with open(inp_file, "r") as inp_json:
        input = json.load(inp_json)
        input["exp_limit"] = np.array(input["exp_limit"]) / norm
        benchmarks.append(input)

limit = np.array(limit) / norm

fig, ax = plt.subplots(1, 1, figsize=(15, 10))
x_min = 0
x_max = np.max([d["n_clusters"] for d in benchmarks]) * 1.1
limit_x = [x_min, x_max]
# mplhep.cms.label(label="Private Work", year=2017, loc=0, fontsize=large)

ax.fill_between(limit_x, limit[3], limit[4], alpha=0.7, color="thistle")
ax.fill_between(limit_x, limit[1], limit[3], alpha=0.7, color=(120 / 256, 33 / 256, 80 / 256))
ax.fill_between(limit_x, limit[1], limit[0], alpha=0.7, color="thistle")
ax.axhline(y=limit[2], color="black", linestyle="dashdot", label="Standard binning (expected)")

ax.errorbar(
    [d["n_clusters"] for d in benchmarks],
    [d["exp_limit"][2] for d in benchmarks],
    yerr=[
        [d["exp_limit"][2] - d["exp_limit"][1] for d in benchmarks],
        [d["exp_limit"][3] - d["exp_limit"][2] for d in benchmarks],
    ],
    marker="o",
    ls="none",
    label="Clustering (expected)",
    color="black",
    capsize=2.0,
)
ax.errorbar(
    [d["n_clusters"] for d in benchmarks],
    [d["exp_limit"][2] for d in benchmarks],
    yerr=[
        [d["exp_limit"][2] - d["exp_limit"][0] for d in benchmarks],
        [d["exp_limit"][4] - d["exp_limit"][2] for d in benchmarks],
    ],
    marker=None,
    ls="none",
    color="black",
    lw=1.0,
)
# 95% C.L. Limit on (ZH)/ (ZH)SM
ax.set_xlim(x_min, x_max)
ax.set_ylim(0.2, 2.3)
ax.set_yticks([0.5, 1.0, 1.5, 2.0])
ax.set_xlabel("# Clusters", fontsize=larger)
ax.set_ylabel(r"Relative Improvement of Sensitivity a.u.", fontsize=larger)
ax.legend()
fig.savefig(os.path.join(args.output_path, "benchmark.png"))
