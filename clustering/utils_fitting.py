import numpy as np
import time
import logging
import pyhf


log = logging.getLogger(__name__)


def make_spec(sigs, bkgs):
    return {
        "channels": [
            {
                "name": "category-test",
                "samples": [
                    {
                        "name": name,
                        "data": list(data),
                        "modifiers": [
                            {
                                "name": "mu",
                                "type": "normfactor",
                                "data": None,
                            },
                            {
                                "name": "statunc",
                                "type": "staterror",
                                "data": unc,
                            },
                        ],
                    }
                    for name, (data, unc) in sigs.items()
                    if np.sum(data) > 0.0
                ]
                + [
                    {
                        "name": name,
                        "data": list(data),
                        "modifiers": [
                            {
                                "name": f"{name}_norm",
                                "type": "normsys",
                                "data": {"hi": 1.2, "lo": 0.8},  # 20% bkg norm
                            },
                            {
                                "name": "statunc",
                                "type": "staterror",
                                "data": unc,
                            },
                        ],
                    }
                    for name, (data, unc) in bkgs.items()
                    if np.sum(data) > 0.0  # needed in case that there are zero events
                ],
            }
        ],
        "parameters": [{"name": "mu", "inits": [1], "bounds": [[0, 100]]}],
    }


def clean_all_bins(tuples, verbose=True):

    mask = np.where(np.sum([np.asarray(arr[0]) for arr in tuples], axis=0) > 1e-3)
    if verbose is True:
        print("Cleaning function deleted {} zero bins".format(len(tuples[0][0]) - len(mask[0])))
    ret = []
    for pair in tuples:
        ret.append((apply_mask(pair[0], mask), apply_mask(pair[1], mask)))
    return ret


def clean_signals_backgrounds(signals, backgrounds, verbose=True):
    all_procs = list(signals.values()) + list(backgrounds.values())
    cleaned_bins = clean_all_bins(all_procs, verbose=verbose)
    signals = dict(zip(signals.keys(), cleaned_bins[: len(signals.keys())]))
    backgrounds = dict(zip(backgrounds.keys(), cleaned_bins[len(signals.keys()) :]))

    return signals, backgrounds


def apply_mask(arr, mask):
    if isinstance(arr, list):
        return list(np.array(arr)[mask])
    elif isinstance(arr, np.ndarray):
        return arr[mask]
    else:
        print(type(arr))
        raise NotImplementedError


def clean_bins(bins, *clean_alike):
    non_zero_entries = np.where(bins > 0.0)
    new_bins = bins[non_zero_entries]
    if clean_alike is not None:
        ret_args = []
        for arr in clean_alike:
            ret_args.append(apply_mask(arr, non_zero_entries))
        return new_bins, *ret_args
    else:
        return new_bins


def silencer():
    import logging
    import warnings

    pyhflog = logging.getLogger("pyhf.optimize.mixins")
    pyhflog.disabled = True

    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        yield

    pyhflog.disabled = False


def limits(sigs, bkgs, verbose=True):

    sigs, bkgs = clean_signals_backgrounds(sigs, bkgs, verbose=verbose)

    orig_sigs = sigs.copy()
    orig_bkgs = bkgs.copy()

    sigs = [s[0] for s in sigs.values()]
    bkgs = [b[0] for b in bkgs.values()]
    data = list(np.sum(np.asarray(bkgs), axis=0) + np.sum(np.asarray(sigs), axis=0))

    pdf = pyhf.Model(make_spec(sigs=orig_sigs, bkgs=orig_bkgs))

    data += pdf.config.auxdata

    # change step size to smaller/finer steps?
    mu_tests = np.arange(0, 20, 0.2)

    try:
        # with silencer():
        _, exp_limits, (_, _) = pyhf.infer.intervals.upperlimit(
            data, pdf, mu_tests, level=0.05, return_results=True
        )
    except Exception as e:
        print("An Exception occured!\n", e)
        exp_limits = np.array(
            [
                np.array(np.nan),
                np.array(np.nan),
                np.array(np.nan),
                np.array(np.nan),
                np.array(np.nan),
            ]
        )

    return exp_limits


def calculate_limits(signals, backgrounds):
    start_counter = time.perf_counter()
    exp_limits = limits(signals, backgrounds)
    end_counter = time.perf_counter()
    log.info("Fitting needed {0:2.0f}s".format(end_counter - start_counter))

    log.info(f"Expected limit is: {exp_limits[2]}")
    log.info(f"1 {chr(963)} interval: {exp_limits[1]} - {exp_limits[3]}")
    log.info(f"2 {chr(963)} interval: {exp_limits[0]} - {exp_limits[4]}")

    return exp_limits
