import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker
import mplhep
from sklearn.manifold import TSNE

plt.rcParams["font.family"] = ["serif"]
plt.rcParams["axes.grid"] = True
plt.rcParams["grid.alpha"] = 0.8
plt.rcParams["axes.axisbelow"] = True
plt.rcParams["axes.linewidth"] = 1.0
plt.rcParams["axes.titleweight"] = "bold"
plt.rcParams["axes.titlecolor"] = "#4A4A4A"
plt.rcParams["patch.linewidth"] = 1.5
plt.rcParams["ytick.right"] = True

larger = 20  # 28
large = 18  # 26
med = 16  # 20

_params = {
    "axes.titlesize": larger,
    "legend.fontsize": med,
    "figure.figsize": (16, 10),
    "axes.labelsize": larger,
    "xtick.labelsize": large,
    "ytick.labelsize": large,
    "figure.titlesize": large,
    "xtick.bottom": True,
    "xtick.direction": "out",
    "ytick.direction": "out",
    "xtick.major.size": 10,
    "ytick.major.size": 10,
    "xtick.minor.size": 6,
    "ytick.minor.size": 6,
    "ytick.left": True,
}
plt.rcParams.update(_params)


colors = {
    "VH": "firebrick",
    "ggZH": "lightcoral",
    "ZH DY": "firebrick",
    "WH": "coral",
    "TT": (117 / 256, 171 / 256, 79 / 256),
    "DY": (40 / 256, 53 / 256, 135 / 256),
    "VV(V)": (55 / 256, 126 / 256, 184 / 256),
    "ST": (244 / 256, 216 / 256, 44 / 256),
    "W + Jets": (199 / 256, 21 / 256, 133 / 256),
    "QCD": (0 / 256, 102 / 256, 102 / 256),
    "Other": (120 / 256, 33 / 256, 80 / 256),
}


def TSNE_visualisation2D(x, x_prediction, n_components=2, outfile="TSNE", lr=1000, v=1):
    x_embed = TSNE(
        n_components=n_components, learning_rate=lr, init="pca", verbose=v
    ).fit_transform(x)
    plt.figure()
    if n_components == 3:
        ax = plt.axes(projection="3d")
        ax.scatter(x_embed[:, 0], x_embed[:, 1], x_embed[:, 2], c=x_prediction, cmap="tab20b")
        # ax.view_init(0, 0)
    else:
        plt.scatter(x_embed[:, 0], x_embed[:, 1], marker=".", c=x_prediction, cmap="tab20b")
        plt.colorbar()
    plt.title("TSNE Embedding")
    plt.tight_layout()
    plt.savefig(f"{outfile}.png")
    plt.close()


def visualisation2D(x_embed, x_prediction, n_components=2, outfile="2D"):
    plt.figure()
    plt.scatter(x_embed[:, 0], x_embed[:, 1], marker=".", c=x_prediction, cmap="inferno")
    cbar = plt.colorbar(ticks=[0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20], orientation="horizontal")
    cbar.ax.get_xaxis().labelpad = 30
    cbar.ax.set_xlabel(r"Cluster")
    plt.clim(0.0, 20.0)
    plt.tick_params(
        axis="both",
        which="both",
        bottom=False,
        top=False,
        right=False,
        left=False,
        labelbottom=False,
        labelleft=False,
    )
    plt.axis(False)
    # plt.title("PCA Decomposition")
    plt.grid(visible=False)
    plt.tight_layout()
    plt.savefig(f"{outfile}.png")
    plt.close()


def cluster_histogram(pred, procid, n_cluster, outfile="signal_clusters"):
    prediction = {
        "All": pred,
        "ggZH": pred[procid == 9421],
        "ZH_DY": pred[procid == 9411],
        "WH": pred[np.any([[procid == 9502], [procid == 9504]], axis=0).flatten()],
    }
    fig, axes = plt.subplots(nrows=2, ncols=len(prediction), sharex=True, figsize=(12, 5))
    bin_edges = np.linspace(-0.5, n_cluster - 0.5, n_cluster + 1)
    bin_center = np.linspace(0, n_cluster - 1, n_cluster)
    for (title, pred), ax in zip(prediction.items(), axes.T):
        ax[0].hist(pred, bins=bin_edges, color="seagreen", alpha=0.8)
        ax[0].set_title(title.replace("_", " "))
        ax[0].set_ylabel("Entries")
        ax[1].hist(pred, bins=bin_edges, color="firebrick", alpha=0.8)
        ax[1].set_xticks(bin_center, minor=True)
        ax[1].set_yscale("log")
        ax[1].grid(which="minor", axis="y", color="#b0b0b0", alpha=0.3, linewidth=0.6)
        ax[1].set_xlabel("Cluster")
        ax[1].set_ylabel("Entries")
    mplhep.cms.label(label="Private Work", loc=0, fontsize=large, ax=ax[0])
    fig.tight_layout()
    fig.savefig(f"{outfile}.png")
    plt.close()


def histogram_n_events(pred, outfile, max_bins=10):

    unique, counts = np.unique(pred, return_counts=True)
    fig, ax = plt.subplots(nrows=1, ncols=1)
    ax.hist(counts, bins=max_bins)
    ax.set_xlabel("Occupation")
    ax.set_ylabel("Entrie")
    ax.set_title("Occupancy per cluster")
    fig.savefig(outfile)


def plot_cluster(
    pred,
    weight,
    bg_processes,
    sig_processes,
    n_cluster,
    colors=colors,
    outfile="signal_clusters",
):
    pred_weight_sorted = sorted(
        [(pred[mask], weight[mask], key) for key, mask in bg_processes.items()],
        key=lambda x: sum(x[1]),
    )
    pred_sorted, weight_sorted, label_sorted = zip(*pred_weight_sorted)

    sig_pred_weight_sorted = sorted(
        [(pred[mask], weight[mask], key) for key, mask in sig_processes.items()],
        key=lambda x: sum(x[1]),
    )
    sig_pred_sorted, sig_weight_sorted, sig_label_sorted = zip(*sig_pred_weight_sorted)

    bin_edges = np.linspace(-0.5, n_cluster - 0.5, n_cluster + 1)
    bin_center = np.linspace(0, n_cluster - 1, n_cluster)
    plot_hist_ratio(
        pred_sorted,
        sig_pred_sorted,
        weight_sorted,
        sig_weight_sorted,
        bin_edges,
        bin_center,
        label_sorted,
        sig_label_sorted,
        "Cluster",
        colors=colors,
        outfile=outfile,
    )


def plot_cluster_i(pred, weight, score, cluster_i, bg_processes, sig_processes, colors=colors):
    weight_i = weight[pred == cluster_i]
    score_i = score[pred == cluster_i]
    score_weight_sorted = sorted(
        [
            (score_i[mask[pred == cluster_i]], weight_i[mask[pred == cluster_i]], key)
            for key, mask in bg_processes.items()
        ],
        key=lambda x: sum(x[1]),
    )
    score_sorted, weight_sorted, label_sorted = zip(*score_weight_sorted)

    sig_score_weight_sorted = sorted(
        [
            (score_i[mask[pred == cluster_i]], weight_i[mask[pred == cluster_i]], key)
            for key, mask in sig_processes.items()
        ],
        key=lambda x: sum(x[1]),
    )
    sig_score_sorted, sig_weight_sorted, sig_label_sorted = zip(*sig_score_weight_sorted)

    n_bins = max(1, int(len(weight_i) / 2500))
    n_bins = min(12, n_bins)
    bins = np.linspace(0.0, 1.0, n_bins + 1)
    bin_width = (bins[-1] - bins[0]) / len(bins)
    bin_center = bins[:-1] + bin_width / 2
    plot_hist_ratio(
        score_sorted,
        sig_score_sorted,
        weight_sorted,
        sig_weight_sorted,
        bins,
        bin_center,
        label_sorted,
        sig_label_sorted,
        "DNN Score",
        colors=colors,
        outfile=f"{output_path}/cluster_{cluster_i}_ratio",
        title=f"Cluster {cluster_i}",
    )


def plot_hist_ratio(
    x_bg,
    x_sig,
    weight_bg,
    weight_sig,
    bins,
    bin_center,
    label_bg,
    label_sig,
    x_label,
    colors,
    outfile,
    title=None,
):
    fig, axes = plt.subplots(
        nrows=2,
        ncols=1,
        sharex=True,
        figsize=(10, 8),
        dpi=500,
        gridspec_kw={"height_ratios": [3, 1]},
    )
    if title:
        axes[0].set_title(title)
    n, bins, patches = axes[0].hist(
        x_bg,
        bins=bins,
        weights=weight_bg,
        stacked=True,
        label=label_bg,
        color=[colors[key] for key in label_bg],
    )
    sig_n, bins, patches = axes[0].hist(
        x_sig,
        bins=bins,
        weights=weight_sig,
        histtype="step",
        stacked=False,
        label=label_sig,
        color=[colors[key] for key in label_sig],
    )
    axes[0].set_yscale("log")
    axes[0].grid(which="minor", axis="y", color="#b0b0b0", alpha=0.3, linewidth=0.6)
    axes[1].grid(which="minor", axis="y", color="#b0b0b0", alpha=0.3, linewidth=0.6)
    axes[0].set_ylim(top=30 * max(n.sum(axis=0)))
    axes[0].set_ylabel("Entries")
    ylim = 0
    if len(label_sig) > 1:
        for j_sig in range(len(label_sig)):
            significance = np.where(
                n.sum(axis=0) + sig_n.sum(axis=0) > 0,
                sig_n[j_sig] / np.sqrt(n.sum(axis=0) + sig_n.sum(axis=0)),
                0,
            )
            ylim = max(ylim, np.max(significance))
            axes[1].scatter(bin_center, significance, color=colors[label_sig[j_sig]])
    elif len(label_sig) == 1:
        j_sig = 0
        significance = np.where(
            n.sum(axis=0) + sig_n > 0,
            sig_n / np.sqrt(n.sum(axis=0) + sig_n),
            0,
        )
        ylim = max(ylim, np.max(significance))
        axes[1].scatter(bin_center, significance, color=colors[label_sig[j_sig]])
    axes[1].set_yscale("log")
    axes[1].set_ylim(3e-3, max(1, 3 * ylim))
    axes[1].set_ylabel(r"$\frac{S}{\sqrt{S\,+\,B}}$", rotation=0, ha="right")
    axes[1].set_xlabel(x_label)
    axes[0].legend(
        loc="upper left",
        # bbox_to_anchor=(0, 1),
        bbox_to_anchor=(0.0, 0.7, 1, 0.3),
        ncol=5,
        # fontsize="large",
        mode="expand",
        # frameon=False,
        # columnspacing=4.5,
    )
    # plt.tick_params(axis="y", which="minor")
    # axes[1].set_xticks(bin_center)  # , minor=True
    locmaj = matplotlib.ticker.LogLocator(base=10, numticks=12)
    axes[0].yaxis.set_major_locator(locmaj)
    locmin = matplotlib.ticker.LogLocator(base=10.0, subs=(0.2, 0.4, 0.6, 0.8), numticks=30)
    axes[0].yaxis.set_minor_locator(locmin)
    axes[0].yaxis.set_minor_formatter(matplotlib.ticker.NullFormatter())
    fig.subplots_adjust(wspace=5, hspace=0)
    mplhep.cms.label(label="Private Work", year=2017, loc=0, fontsize=large, ax=axes[0])
    axes[0].set_xlim(bins.min(), bins.max())
    plt.tight_layout()
    plt.savefig(f"{outfile}.png")
    if bins.max() > 25:
        axes[0].set_xlim(bins.max() - 25, bins.max())
        plt.savefig(f"{outfile}_last25.png")
    if bins.max() > 50:
        axes[0].set_xlim(bins.max() - 50, bins.max())
        plt.savefig(f"{outfile}_last50.png")
    plt.close()
