import os
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, roc_auc_score
from plotting import plot_roc, plot_metric


out_dir = "plots"
os.makedirs(out_dir, exist_ok=True)
#DATA_PATH = '/net/scratch/cms/dihiggs/store/vh/Run2_pp_13TeV_2017/CoffeaProcessor/dev1/vh/selection/ExporterProcessor/'

try:
    data_ZH_hl = np.load(os.path.join(os.getenv("DATA_PATH"), "ZH_bbll+mumu_2b+hl.npy"))
    data_ZH_lep = np.load(
        os.path.join(os.getenv("DATA_PATH"), "ZH_bbll+mumu_2b+lep.npy")
    )
    data_ZH_jet = np.load(
        os.path.join(os.getenv("DATA_PATH"), "ZH_bbll+mumu_2b+jet.npy")
    )
    data_ggZH_hl = np.load(
        os.path.join(os.getenv("DATA_PATH"), "ggZH_bbll+mumu_2b+hl.npy")
    )
    data_ggZH_jet = np.load(
        os.path.join(os.getenv("DATA_PATH"), "ggZH_bbll+mumu_2b+jet.npy")
    )
    data_ggZH_lep = np.load(
        os.path.join(os.getenv("DATA_PATH"), "ggZH_bbll+mumu_2b+lep.npy")
    )
    

except FileNotFoundError:
    print("Your file was not found!")
    
data_ZH = np.concatenate(
    (
        data_ZH_hl,
        data_ZH_lep.reshape(data_ZH_lep.shape[0], -1),
        data_ZH_jet.reshape(data_ZH_jet.shape[0], -1),
    ),
    axis=1,
)
data_ggZH = np.concatenate(
    (
        data_ggZH_hl,
        data_ggZH_lep.reshape(data_ggZH_lep.shape[0], -1),
        data_ggZH_jet.reshape(data_ggZH_jet.shape[0], -1),
    ),
    axis=1,
)


print("~~~~~~")
print("Signalfraction")
print(len(data_ggZH)/(len(data_ZH)/2))

#test samples
data_ZH, test_ZH = train_test_split(data_ZH, test_size=0.2, random_state=42)
data_ggZH, test_ggZH = train_test_split(data_ggZH, test_size=0.2, random_state=42)

#create mixed samples
data_mixed = np.concatenate((data_ZH[:int(len(data_ZH)/2)], data_ggZH), axis=0)
data_background = data_ZH[int(len(data_ZH)/2):]


labels_mixed = np.zeros((data_mixed.shape[0], 2))
labels_mixed[..., 0] = 1
labels_back = np.zeros((data_background.shape[0], 2))
labels_back[..., 1] = 1

data = np.concatenate( (data_mixed, data_background), axis=0)
labels = np.concatenate( (labels_mixed, labels_back), axis=0)

X_train, X_val, y_train, y_val = train_test_split(data, labels, test_size=0.2, random_state=42)


# Preprocessing
data = np.true_divide(np.subtract(data, np.mean(data, axis=0)), np.std(data, axis=0))


# build model

L2 = 1e-3

model = tf.keras.Sequential([
    tf.keras.layers.Flatten(input_shape=data.shape[1:]),
    tf.keras.layers.Dense(200,kernel_regularizer=tf.keras.regularizers.l2(L2), activation='relu'),
    tf.keras.layers.Dropout(0.33),
    tf.keras.layers.Dense(200,kernel_regularizer=tf.keras.regularizers.l2(L2), activation='relu'),
    tf.keras.layers.Dropout(0.33),
    tf.keras.layers.Dense(200,kernel_regularizer=tf.keras.regularizers.l2(L2), activation='relu'),
    tf.keras.layers.Dropout(0.33),
    tf.keras.layers.Dense(2, activation='softmax')
])

model.compile(optimizer='adam',
        loss=tf.keras.losses.CategoricalCrossentropy(from_logits=False),
              metrics=[tf.keras.metrics.BinaryAccuracy(name='accuracy'),
                       tf.keras.metrics.AUC(name='auc')],
              )

print(model.summary())

lr_callback = tf.keras.callbacks.ReduceLROnPlateau(
        monitor='val_loss', factor=0.1, patience=10, verbose=1,
        mode='auto', min_delta=0.0001, cooldown=20, min_lr=0
)

summary = model.fit(X_train, y_train, epochs=30, batch_size=128,
              validation_data=(X_val, y_val),
              #class_weight=class_weight,
              callbacks=[lr_callback]
        ) 
history = summary.history

"""
# Evaluation

"""
ZH_pred = model.predict(test_ZH)
ggZH_pred = model.predict(test_ggZH)

bins = 20

print(ZH_pred.shape)
print(ggZH_pred.shape)

plt.figure()
plt.hist(ZH_pred[...,0], bins=bins, density=True, alpha=0.5, label="ZH (DY-like)")
plt.hist(ggZH_pred[...,0], bins=bins, density=True, alpha=0.5, label="ggZH")
plt.xlabel("Score")
plt.ylabel("Events (normed)")
#plt.xlim(0.0, 1.0)
plt.legend(loc="upper right")
# plt.yscale("log")
plt.savefig("plots/cwola_score_hist.png")


#evaluation = model.evaluate(X_test, y_test)

plt.figure()
ax_epochs = range(0,len(history["loss"]))
plt.plot(ax_epochs, history["loss"], color='g', label='Training')
plt.plot(ax_epochs,history["val_loss"], color='b', label='Validation')
plt.xlabel("Epochs")
plt.ylabel("Loss")
#plt.yscale("log")
plt.legend(loc="upper right")
plt.grid()
plt.tight_layout()
plt.savefig("plots/cwola_loss.png")


