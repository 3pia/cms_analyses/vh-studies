import argparse
import logging
import os
import numpy as np

from sklearn.cluster import KMeans

from run_clustering import load_data, preprocess_logits
from plotting import TSNE_visualisation2D


def main(input_path, output_path, input_centers, n_clusters=None, categories=None, verbose=True):

    logits, procid, weight = load_data(input_path, categories, verbose)
    logits = preprocess_logits(logits)

    centers = np.load(input_centers)

    kmeans = KMeans(n_clusters=n_clusters, init=centers)
    kmeans.fit(centers) # needs to be done to init the object   kmeans = 

    prediction = kmeans.predict(logits)

    TSNE_visualisation2D(logits, prediction, n_components=2, outfile=os.path.join(output_path, "tsne_embedding.png"))



if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Clustering algorithm')
    parser.add_argument('input_path', metavar='i', type=str,
                        help='input path of LogitExporterProcessor')
    parser.add_argument('output_path', metavar='o', type=str,
                        help='output path of Clustering')
    parser.add_argument('input_centers', metavar='i', type=str,
                        help='input path of cluster centers')
    parser.add_argument('n_clusters', type=int, help='Integer number of clusters to use')
    parser.add_argument('--categories', default=["mumu_2b"], nargs='*', help='Categories to use')
    parser.add_argument('--verbose', default=1, type=int, help="Verbosity level")

    args = parser.parse_args()
    # print(args)
    os.makedirs(args.output_path, exist_ok=True)

    FORMAT = 'Clustering: %(levelname)s %(message)s'
    logging.basicConfig(level=10 if args.verbose == 2 else 20 if args.verbose == 1 else 30, format=FORMAT)

    main(args.input_path, args.output_path, args.input_centers, n_clusters=args.n_clusters, categories=args.categories)