import argparse
import logging
import os
import time
import numpy as np
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA

from run_clustering import load_data, preprocess_logits

log = logging.getLogger(__name__)


def TSNE_embedding(x, n_components=2, outfile="TSNEembedding.npy", lr=1000, v=1):
    x_embed = TSNE(
        n_components=n_components,
        learning_rate=lr,
        init="pca",
        perplexity=30,
        verbose=v,
    ).fit_transform(x)
    np.save(outfile, x_embed)


def PCA_embedding(x, n_components=2, outfile="PCA.npy", lr=1000, v=1):
    x_embed = PCA(n_components=2).fit_transform(x)
    np.save(outfile, x_embed)


def main(input_path, output_path, categories=None, tsne=False, verbose=True):

    logits, procid, weight = load_data(input_path, categories, verbose)
    logits = preprocess_logits(logits, use_softmax=True)

    start_counter = time.perf_counter()
    if tsne:
        TSNE_embedding(
            logits,
            n_components=2,
            outfile=os.path.join(output_path, "TSNEembedding.npy"),
        )

    else:
        PCA_embedding(
            logits,
            n_components=2,
            outfile=os.path.join(output_path, "PCA.npy"),
        )
    end_counter = time.perf_counter()
    log.info("Embedding needed {0:2.0f}s".format(end_counter - start_counter))


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Clustering algorithm")
    parser.add_argument(
        "input_path", metavar="i", type=str, help="input path of LogitExporterProcessor"
    )
    parser.add_argument("output_path", metavar="o", type=str, help="output path of Clustering")
    parser.add_argument("--categories", default=["mumu_2b"], nargs="*", help="Categories to use")
    parser.add_argument("--tsne", default=False, nargs="*", help="TSNE embedding rather than PCA")
    parser.add_argument("--verbose", default=1, type=int, help="Verbosity level")

    args = parser.parse_args()
    os.makedirs(args.output_path, exist_ok=True)

    FORMAT = "Clustering: %(levelname)s %(message)s"
    logging.basicConfig(
        level=10 if args.verbose == 2 else 20 if args.verbose == 1 else 30,
        format=FORMAT,
    )

    main(
        args.input_path,
        args.output_path,
        categories=args.categories,
        tsne=args.tsne,
    )
