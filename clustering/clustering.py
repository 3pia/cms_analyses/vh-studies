import logging
import time
import numpy as np

from sklearn.cluster import KMeans
from sklearn.cluster import MiniBatchKMeans

log = logging.getLogger(__name__)

def clustering(logits, procid, weights, n_clusters=50, max_iter=100, verbose=True, **kwargs):
    log.info(f"Start clustering with {n_clusters} clusters and {max_iter} max-iterations")
    start_counter = time.perf_counter()
    kmeans_args = {
        "n_clusters": n_clusters,
        "max_iter": max_iter,
        "init": "random",
        "random_state": 0,
        "tol": 1e-5,
        # verbose:1,
    }
    if kwargs.get("MiniBatch", False) is True:
        kmeans_func = MiniBatchKMeans
        kmeans_args["batch_size"] = kwargs.get("batch_size", 1024)
    else:
        kmeans_func = KMeans
        kmeans_args["algorithm"] = "full"

    kmeans = kmeans_func(**kmeans_args).fit(logits, sample_weight=weights)

    end_counter = time.perf_counter()
    log.info("Clustering needed {0:2.0f}s".format(end_counter - start_counter))

    return kmeans

def binning_from_clusters(
    pred,
    weight,
    bg_processes,
    sig_processes,
    n_cluster,
):
    pred_weight_sorted = sorted(
        [(pred[mask], weight[mask], key) for key, mask in bg_processes.items()],
        key=lambda x: sum(x[1]),
    )
    pred_sorted, weight_sorted, label_sorted = zip(*pred_weight_sorted)

    sig_pred_weight_sorted = sorted(
        [(pred[mask], weight[mask], key) for key, mask in sig_processes.items()],
        key=lambda x: sum(x[1]),
    )
    sig_pred_sorted, sig_weight_sorted, sig_label_sorted = zip(*sig_pred_weight_sorted)

    bin_edges = np.linspace(-0.5, n_cluster - 0.5, n_cluster + 1)
    bin_center = np.linspace(0, n_cluster - 1, n_cluster)

    backgrounds_unweighted = [np.histogram(pred, bin_edges) for pred in zip(pred_sorted)]
    signals_unweighted = [np.histogram(pred, bin_edges) for pred in zip(sig_pred_sorted)]

    # values need to be at least 1e-5
    backgrounds = [
        np.histogram(pred, bin_edges, weights=weight)
        for pred, weight in zip(pred_sorted, weight_sorted)
    ]
    signals = [
        np.histogram(pred, bin_edges, weights=weight)
        for pred, weight in zip(sig_pred_sorted, sig_weight_sorted)
    ]

    background_uncs = [
        [
            np.sqrt(np.sum(weight[idx == i] ** 2))
            if np.sqrt(np.sum(weight[idx == i] ** 2)) > 1e-5
            else 1e-5
            for i in range(len(bin_center))
        ]
        for weight, idx in zip(weight_sorted, pred_sorted)
    ]
    signal_uncs = [
        [
            np.sqrt(np.sum(weight[idx == i] ** 2))
            if np.sqrt(np.sum(weight[idx == i] ** 2)) > 1e-5
            else 1e-5
            for i in range(len(bin_center))
        ]
        for weight, idx in zip(sig_weight_sorted, sig_pred_sorted)
    ]

    signals = {
        label: (data[0], uncs) for label, data, uncs in zip(sig_label_sorted, signals, signal_uncs)
    }

    backgrounds = {
        label: (data[0], uncs)
        for label, data, uncs in zip(label_sorted, backgrounds, background_uncs)
    }
    return signals, backgrounds