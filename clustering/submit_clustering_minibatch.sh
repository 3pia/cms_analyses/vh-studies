#!/bin/bash
# inpdir=/net/scratch/cms/dihiggs/store/vh/Run2_pp_13TeV_2017/CoffeaProcessor/run_01/vh/selection/LogitExporterProcessor/
base_directory=output/ul_prod_03_semi_incl_01
# inpdir=/net/scratch/cms/dihiggs/store/vh/Run2_pp_13TeV_UL_2017/CoffeaProcessor/prod_02/selection/vh/selection/LogitExporterProcessor/
inpdir=/net/scratch/cms/dihiggs/store/vh/Run2_pp_13TeV_UL_2017/CoffeaProcessor/prod_03/selection/vh/selection/LogitExporterProcessor/
maxiter=10000
MEMORY=15000
# categories="ee_boosted_1b mumu_boosted_1b e_boosted_1b mu_boosted_1b ee_resolved_1b ee_resolved_2b mumu_resolved_1b mumu_resolved_2b e_resolved_2b e_resolved_1b mu_resolved_2b mu_resolved_1b"

# declare -a CategoryArray=("ee_boosted_1b mumu_boosted_1b e_boosted_1b mu_boosted_1b ee_resolved_1b ee_resolved_2b mumu_resolved_1b mumu_resolved_2b e_resolved_2b e_resolved_1b mu_resolved_2b mu_resolved_1b" "ee_boosted_1b mumu_boosted_1b ee_resolved_1b ee_resolved_2b mumu_resolved_1b mumu_resolved_2b" "ee_boosted_1b mumu_boosted_1b ee_resolved_1b ee_resolved_2b mumu_resolved_1b mumu_resolved_2b e_boosted_1b mu_boosted_1b" "ee_boosted_1b mumu_boosted_1b e_boosted_1b mu_boosted_1b" "ee_resolved_1b ee_resolved_2b mumu_resolved_1b mumu_resolved_2b e_resolved_2b e_resolved_1b mu_resolved_2b mu_resolved_1b")
declare -A Cats
Cats["incl"]="incl"
# Cats["incl_1L"]="incl_1L"
# Cats["incl_2L"]="incl_2L"
# Cats["incl_boosted"]="incl_boosted"
# Cats["incl_res"]="incl_res"

mkdir $directory
for cat in "${!Cats[@]}";do
    tag="${cat// /_}"
    echo $tag
    directory=$base_directory/$tag
    mkdir $directory

    submit -m $MEMORY -L $base_directory/logs python3 run_clustering.py $inpdir $directory --n-clusters 50 --categories ${Cats[$cat]} --verbose 1 --max-iter $maxiter --mini-batch --no-limit --batch-size 1024 --tag $cat 
    submit -m $MEMORY -L $base_directory/logs python3 run_clustering.py $inpdir $directory --n-clusters 100 --categories ${Cats[$cat]} --verbose 1 --max-iter $maxiter --mini-batch --no-limit --batch-size 1024 --tag $cat 
    submit -m $MEMORY -L $base_directory/logs python3 run_clustering.py $inpdir $directory --n-clusters 150 --categories ${Cats[$cat]} --verbose 1 --max-iter $maxiter --mini-batch --no-limit --batch-size 1024 --tag $cat 
    submit -m $MEMORY -L $base_directory/logs python3 run_clustering.py $inpdir $directory --n-clusters 250 --categories ${Cats[$cat]} --verbose 1 --max-iter $maxiter --mini-batch --no-limit --batch-size 1024 --tag $cat
    submit -m $MEMORY -L $base_directory/logs python3 run_clustering.py $inpdir $directory --n-clusters 500 --categories ${Cats[$cat]} --verbose 1 --max-iter $maxiter --mini-batch --no-limit --batch-size 1024 --tag $cat 
    submit -m $MEMORY -L $base_directory/logs python3 run_clustering.py $inpdir $directory --n-clusters 1000 --categories ${Cats[$cat]} --verbose 1 --max-iter $maxiter --mini-batch --no-limit --batch-size 1024 --tag $cat 
done