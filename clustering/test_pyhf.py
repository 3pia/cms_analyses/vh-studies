import argparse
import pyhf
import os
import numpy as np
import time
import json

from coffea.util import load

from fitting import limits, clean_signals_backgrounds


def main(input_path, output_path, categories, split_signal=False):

    # "/net/scratch/cms/dihiggs/store/vh/Run2_pp_13TeV_2017/Rebin/run_01/selection/02f81aabde84db10aeedd0a51cd4c82fbedbfb45d25d235e5b53627bfeb9b958/vh-model/StatModel/rebinned_hists.coffea"
    # "/net/scratch/cms/dihiggs/store/vh/Run2_pp_13TeV_UL_2017/Rebin/prod01/selection/614624e99fd74f788911b1cda7a396b958624feb1ad996603074216259ef809b/vh_new/StatModel/rebinned_hists.coffea"
    categories = [
        # "e_boosted_1b",  # failed?
        # "ee_boosted_1b", # failed
        "mu_boosted_1b",  # works
        "mumu_boosted_1b",  # works
        "e_resolved_1b",  # works
        "ee_resolved_1b",  # works
        "mu_resolved_1b",  # works
        "mumu_resolved_1b",  # works
        "e_resolved_2b",  # works
        "ee_resolved_2b",  # works
        "mu_resolved_2b",  # works
        "mumu_resolved_2b",  # works
    ]
    if not isinstance(categories, list):
        categories = [categories]
    f = load(input_path)

    bg_processes = {
        "tt": 1,
        "dy": 1,
        "st": 1,
        "wjets": 1,
        "QCD_TuneCP5": 1,
        "VBFH": 1,
        "ggH": 1,
        # "rare": 1,
        "ttV": 1,
        "ttH": 1,
        "ttVH": 1,
        "ttVV": 1,
        "vv": 1,
        "vvv": 1,
    }
    sig_processes = {
        "ggZH": 1,
        "ZH_DY": 1,
        "WH": 1,
    }

    # if split_signal:
    #     dnn_nodes = [
    #         "ggZH",
    #         "ZH_DY",
    #         "WH",
    #         "QCD",
    #         "VBFH",
    #         "dy",
    #         "ggH",
    #         "merged_multiboson",
    #         "merged_ttVX",
    #         "rare",
    #         "st",
    #         "tt",
    #         "wjets",
    #     ]
    # else:
    dnn_nodes = [
        # signal
        "VH",
        # top related
        "tt",
        "st",
        "ttX",  # ttV, ttVH, ttVV, ttH
        # bosonic
        "dy",
        "wjets",
        "multiboson",  # vv, vvv
        # other
        "class_other_merged",  # rare, ggH, VBFH, QCD
    ]

    # test broken nodes
    # empty bins, should not happen after rebin?
    sig = [
        np.concatenate(
            [
                (f["dnn_score_max"][f"{category}_dnn_node_{node}"][f"{x}", "nominal", ...].values())
                for node in dnn_nodes
                for category in categories
            ],
            axis=0,
        )
        for x in sig_processes.keys()
    ]
    sig_unc = []
    signals = {
        x: (
            np.concatenate(
                [
                    f["dnn_score_max"][f"{category}_dnn_node_{node}"][
                        f"{x}", "nominal", ...
                    ].values()
                    for node in dnn_nodes
                    for category in categories
                ],
                axis=0,
            ),
            list(
                np.concatenate(
                    [
                        np.where(
                            np.sqrt(
                                f["dnn_score_max"][f"{category}_dnn_node_{node}"][
                                    f"{x}", "nominal", ...
                                ].variances()
                            )
                            < 1e-5,
                            1e-5,
                            np.sqrt(
                                f["dnn_score_max"][f"{category}_dnn_node_{node}"][
                                    f"{x}", "nominal", ...
                                ].variances()
                            ),
                        )
                        for node in dnn_nodes
                        for category in categories
                    ],
                    axis=0,
                )
            ),
        )
        for x in sig_processes.keys()
    }
    backgrounds = {
        x: (
            np.concatenate(
                [
                    f["dnn_score_max"][f"{category}_dnn_node_{node}"][
                        f"{x}", "nominal", ...
                    ].values()
                    for node in dnn_nodes
                    for category in categories
                ],
                axis=0,
            ),
            list(
                np.concatenate(
                    [
                        np.where(
                            np.sqrt(
                                f["dnn_score_max"][f"{category}_dnn_node_{node}"][
                                    f"{x}", "nominal", ...
                                ].variances()
                            )
                            < 1e-5,
                            1e-5,
                            np.sqrt(
                                f["dnn_score_max"][f"{category}_dnn_node_{node}"][
                                    f"{x}", "nominal", ...
                                ].variances()
                            ),
                        )
                        for node in dnn_nodes
                        for category in categories
                    ],
                    axis=0,
                )
            ),
        )
        for x in bg_processes.keys()
    }

    # from IPython import embed;embed()
    bins = len(backgrounds["tt"][0])
    print(f"Number of bins: {bins}")

    start_counter = time.perf_counter()
    exp_limits = limits(signals, backgrounds)
    end_counter = time.perf_counter()
    print("Fitting needed {0:2.0f}s".format(end_counter - start_counter))

    print(f"Expected limit is: {exp_limits[2]}")
    print(f"1 {chr(963)} interval: {exp_limits[1]} - {exp_limits[3]}")
    print(f"2 {chr(963)} interval: {exp_limits[0]} - {exp_limits[4]}")

    with open(os.path.join(output_path, "oldmethod_limits.json"), "w") as limits_file:
        out = {
            "exp_limit": list(map(float, exp_limits)),
        }
        json.dump(out, limits_file)


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Clustering algorithm")
    parser.add_argument("input_path", metavar="i", type=str, help="input path of files")
    parser.add_argument("output_path", metavar="o", type=str, help="output path of Limits")
    parser.add_argument("--categories", default=["mumu_2b"], nargs="*", help="Categories to use")
    parser.add_argument(
        "--split-signal", default=False, action="store_true", help="Categories to use"
    )
    # parser.add_argument("--backend", default="NumPy", help="Change backend for testing purposes")
    # parser.add_argument("--precision", default="32B", help="Change precision for testing purposes")
    parser.add_argument("--verbose", default=1, type=int, help="Verbosity level")

    args = parser.parse_args()

    # pyhf.set_backend(args.backend, precision=args.precision)

    main(args.input_path, args.output_path, args.categories, args.split_signal)
