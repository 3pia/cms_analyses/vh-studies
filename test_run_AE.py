import os
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, roc_auc_score
from plotting import plot_roc, plot_metric
import argparse

parser = argparse.ArgumentParser()



parser.add_argument("-v", "--version", default=0, help="Verison to save file, i.e. number", type=str)
parser.add_argument("-b", "--bottleneck", default=4, help="Number of Nodes for the bottleneck", type=int)
parser.add_argument("-e", "--epochs", default=50, help="Number of epochs for training", type=int)
args = parser.parse_args()

bottelneck = args.bottleneck 
epochs = args.epochs
out_dir = "plots/AE_{}/".format(args.version)

os.makedirs(out_dir, exist_ok=True)
#DATA_PATH = '/net/scratch/cms/dihiggs/store/vh/Run2_pp_13TeV_2017/CoffeaProcessor/dev1/vh/selection/ExporterProcessor/'
try:
    data_ZH_hl = np.load(os.path.join(os.getenv("DATA_PATH"), "ZH_bbll+mumu_2b+hl.npy"))
    data_ZH_lep = np.load(
        os.path.join(os.getenv("DATA_PATH"), "ZH_bbll+mumu_2b+lep.npy")
    )
    data_ZH_jet = np.load(
        os.path.join(os.getenv("DATA_PATH"), "ZH_bbll+mumu_2b+jet.npy")
    )
    data_ggZH_hl = np.load(
        os.path.join(os.getenv("DATA_PATH"), "ggZH_bbll+mumu_2b+hl.npy")
    )
    data_ggZH_jet = np.load(
        os.path.join(os.getenv("DATA_PATH"), "ggZH_bbll+mumu_2b+jet.npy")
    )
    data_ggZH_lep = np.load(
        os.path.join(os.getenv("DATA_PATH"), "ggZH_bbll+mumu_2b+lep.npy")
    )
    #data_tt_hl = np.load(os.path.join(os.getenv("DATA_PATH"), "tt_dl+mumu_2b+hl.npy"))
    #data_tt_jet = np.load(os.path.join(os.getenv("DATA_PATH"), "tt_dl+mumu_2b+jet.npy"))
    #data_tt_lep = np.load(os.path.join(os.getenv("DATA_PATH"), "tt_dl+mumu_2b+lep.npy"))

except FileNotFoundError:
    print("Your file was not found!")


data_ZH = np.concatenate(
    (
        data_ZH_hl,
        data_ZH_lep.reshape(data_ZH_lep.shape[0], -1),
        data_ZH_jet.reshape(data_ZH_jet.shape[0], -1),
    ),
    axis=1,
)
data_ggZH = np.concatenate(
    (
        data_ggZH_hl,
        data_ggZH_lep.reshape(data_ggZH_lep.shape[0], -1),
        data_ggZH_jet.reshape(data_ggZH_jet.shape[0], -1),
    ),
    axis=1,
)

# Preprocessing
data_ggZH = (data_ggZH - np.mean(data_ZH)) / np.std(data_ZH)
#data_tt = (data_tt - np.mean(data_ZH)) / np.std(data_ZH)
data_ZH = (data_ZH - np.mean(data_ZH)) / np.std(data_ZH)

X_train, X_test = train_test_split(data_ZH, test_size=0.2, random_state=42)
# X_val, X_test = train_test_split(X_test, test_size=0.5, random_state=42)


# build model


model = tf.keras.Sequential(
    [
        tf.keras.layers.Input(shape=X_train.shape[1:]),
        tf.keras.layers.Dense(64, activation="relu"),
        tf.keras.layers.Dropout(rate=0.2),
        tf.keras.layers.Dense(64, activation="relu"),
        tf.keras.layers.Dropout(rate=0.2),
        tf.keras.layers.Dense(32, activation="relu"),
        tf.keras.layers.Dropout(rate=0.2),
        tf.keras.layers.Dense(32, activation="relu"),
        tf.keras.layers.Dropout(rate=0.2),
        tf.keras.layers.Dense(16, activation="relu"),
        tf.keras.layers.Dropout(rate=0.2),
        tf.keras.layers.Dense(16, activation="relu"),
        tf.keras.layers.Dropout(rate=0.2),

        tf.keras.layers.Dense(bottelneck, activation="relu"),

        tf.keras.layers.Dense(16, activation="relu"),
        tf.keras.layers.Dropout(rate=0.2),
        tf.keras.layers.Dense(16, activation="relu"),
        tf.keras.layers.Dropout(rate=0.2),
        tf.keras.layers.Dense(32, activation="relu"),
        tf.keras.layers.Dense(32, activation="relu"),
        tf.keras.layers.Dense(32, activation="relu"),
        tf.keras.layers.Dense(X_train.shape[1:], activation="relu"),
    ]
)

model.compile(
    optimizer=tf.keras.optimizers.Adam(learning_rate=0.001),
    loss="mse",
)

print(model.summary())

lr_callback = tf.keras.callbacks.ReduceLROnPlateau(
    monitor="val_loss",
    factor=0.1,
    patience=20,
    verbose=1,
    mode="auto",
    min_delta=0.0001,
    cooldown=20,
    min_lr=0,
)

summary = model.fit(
    X_train,
    X_train,
    epochs=epochs,
    batch_size=128,
    validation_data=(X_test, X_test),
    callbacks=[lr_callback],
)
history = summary.history
"""
Evaluation

"""
pred_train = model.predict(X_train)
pred_ZH = model.predict(X_test)
pred_ggZH = model.predict(data_ggZH)
#pred_tt = model.predict(data_tt)

train_mae_loss = np.mean(np.abs(pred_train - X_train), axis=1)
ZH_mae_loss = np.mean(np.abs(pred_ZH - X_test), axis=1)
ggZH_mae_loss = np.mean(np.abs(pred_ggZH - data_ggZH), axis=1)
#tt_mae_loss = np.mean(np.abs(pred_tt - data_tt), axis=1)

plot_metric(history["loss"], history["val_loss"], "loss", out_dir, log=True)

# Plots
bins = np.linspace(0.2, 1.0, 60)

plt.figure()
plt.hist(train_mae_loss, bins=bins, density=True, alpha=0.5, label="Train-Data")
nZH, binsZH, patches = plt.hist(ZH_mae_loss, bins=bins, density=True, alpha=0.5, label="ZH (DY-like)")
nggZH, binsggZH, patches = plt.hist(ggZH_mae_loss, bins=bins, color="red", density=True, alpha=0.5, label="ggZH")
plt.xlabel("MAE loss")
plt.ylabel("Events (normed)")
plt.xlim(0.2, 1.0)
plt.legend(loc="upper right")
# plt.yscale("log")
plt.savefig(os.path.join(out_dir, "loss_hist_{}.pdf".format(bottelneck)))

fig, (ax1, ax2) = plt.subplots(2, sharex=True)
ax1.hist(ZH_mae_loss, bins=bins, density=True, alpha=0.5, label="ZH (DY-like)")
ax1.hist(ggZH_mae_loss, bins=bins, color="red", density=True, alpha=0.5, label="ggZH")
ax1.set_ylabel("Events (normed)")
ax1.legend(loc="upper right")
ax2.plot(bins[:-1] + (bins[-1] - bins[0]) / len(bins), np.nan_to_num(nggZH / nZH), "ko")
# ax2.semilogy()
ax2.set_ylabel("Ratio")
ax2.set_xlabel("MAE loss")
ax2.set_ylim(-0.1, 3.1)
plt.savefig(os.path.join(out_dir, "loss_hist_ratio_{}.png".format(bottelneck)))
