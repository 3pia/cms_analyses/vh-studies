import numpy as np

# import matplotlib.pyplot as plt
# import matplotlib.ticker
import json
from sklearn.cluster import KMeans

# from sklearn.manifold import TSNE
from scipy.special import softmax
from pathlib import Path
import time


def binning_from_clusters(
    pred,
    weight,
    bg_processes,
    sig_processes,
    n_cluster,
):
    pred_weight_sorted = sorted(
        [(pred[mask], weight[mask], key) for key, mask in bg_processes.items()],
        key=lambda x: sum(x[1]),
    )
    pred_sorted, weight_sorted, label_sorted = zip(*pred_weight_sorted)

    sig_pred_weight_sorted = sorted(
        [(pred[mask], weight[mask], key) for key, mask in sig_processes.items()],
        key=lambda x: sum(x[1]),
    )
    sig_pred_sorted, sig_weight_sorted, sig_label_sorted = zip(*sig_pred_weight_sorted)

    bin_edges = np.linspace(-0.5, n_cluster - 0.5, n_cluster + 1)
    bin_center = np.linspace(0, n_cluster - 1, n_cluster)

    backgrounds_unweighted = [np.histogram(pred, bin_edges) for pred in zip(pred_sorted)]
    signals_unweighted = [np.histogram(pred, bin_edges) for pred in zip(sig_pred_sorted)]

    # values need to be at least 1e-5
    backgrounds = [
        np.histogram(pred, bin_edges, weights=weight)
        for pred, weight in zip(pred_sorted, weight_sorted)
    ]
    signals = [
        np.histogram(pred, bin_edges, weights=weight)
        for pred, weight in zip(sig_pred_sorted, sig_weight_sorted)
    ]

    # for b in backgrounds:
    #     b[0][b[0] <= 0] = 1e-5
    # for s in signals:
    #     s[0][s[0] <= 0] = 1e-5
    # backgrounds = [ [b if b > 0 else 1e-5 for b in bb] for bb in backgrounds]
    # signals = [ [s if s>0 else 1e-5 for s in ss] for ss in signals]

    # background_idxs = [
    #     np.digitize(pred, bin_edges) for pred, weight in zip(pred_sorted, weight_sorted)
    # ]
    # signal_idxs = [
    #     np.digitize(pred, bin_edges) for pred, weight in zip(sig_pred_sorted, sig_weight_sorted)
    # ]

    # background_uncs = [
    #     [
    #         np.sqrt(mc[0][i]) * np.sum(weight[idx == i] ** 2)
    #         if mc[0][i] * np.sum(weight[idx == i] ** 2) > 1e-5
    #         else 1e-5
    #         for i in range(len(bin_center))
    #     ]
    #     for mc, weight, idx in zip(backgrounds_unweighted, weight_sorted, background_idxs)
    # ]
    # signal_uncs = [
    #     [
    #         np.sqrt(mc[0][i]) * np.sum(weight[idx == i] ** 2)
    #         if mc[0][i] * np.sum(weight[idx == i] ** 2) > 1e-5
    #         else 1e-5
    #         for i in range(len(bin_center))
    #     ]
    #     for mc, weight, idx in zip(signals_unweighted, sig_weight_sorted, signal_idxs)
    # ]
    background_uncs = [
        [
            np.sqrt(np.sum(weight[idx == i] ** 2))
            if np.sqrt(np.sum(weight[idx == i] ** 2)) > 1e-5
            else 1e-5
            for i in range(len(bin_center))
        ]
        for weight, idx in zip(weight_sorted, pred_sorted)
    ]
    signal_uncs = [
        [
            np.sqrt(np.sum(weight[idx == i] ** 2))
            if np.sqrt(np.sum(weight[idx == i] ** 2)) > 1e-5
            else 1e-5
            for i in range(len(bin_center))
        ]
        for weight, idx in zip(sig_weight_sorted, sig_pred_sorted)
    ]

    signals = {
        label: (data[0], uncs) for label, data, uncs in zip(sig_label_sorted, signals, signal_uncs)
    }

    backgrounds = {
        label: (data[0], uncs)
        for label, data, uncs in zip(label_sorted, backgrounds, background_uncs)
    }
    return signals, backgrounds


def sort_bins(pred, weight, n_cluster, sig_processes):
    signal_mask = np.any([mask for mask in sig_processes.values()], axis=0)
    sig_pred = pred[signal_mask]
    sig_weight = weight[signal_mask]
    sig_weight_sum = [sig_weight[sig_pred == i_cluster].sum() for i_cluster in range(n_cluster)]

    sorting = np.argsort(sig_weight_sum)
    sorted_pred = np.full_like(pred, -1)
    for i in range(n_cluster):
        np.place(sorted_pred, pred == sorting[i], i)
    return sorted_pred


if __name__ == "__main__":
    np.seterr(all="ignore")
    tic = time.time()
    categories = ["mumu_2b"]  # "ee_2b", "mumu_1fb", "ee_1fb"]

    input_path = "/net/scratch/cms/dihiggs/store/vh/Run2_pp_13TeV_2017/CoffeaProcessor/run_01/vh/selection/LogitExporterProcessor/"
    output_path = f"/net/scratch/cms/dihiggs/store/vh/vh-studies/results/"
    Path(output_path).mkdir(parents=True, exist_ok=True)

    print("Reading in logits")
    logits, procid, weight = [], [], []
    for category in categories:
        logits.append(np.load(input_path + f"{category}_logit.npy"))
        procid.append(np.load(input_path + f"{category}_procid.npy").flatten())
        weight.append(np.load(input_path + f"{category}_weight.npy"))

    logits = np.concatenate(logits, axis=0)
    procid = np.concatenate(procid, axis=0)
    weight = np.concatenate(weight, axis=0)
    print("finished reading in logits")
    print(f"Categories: {categories}")
    print("In total {} events".format(len(procid)))

    scores = softmax(logits, axis=1)
    ZH_dy_score = scores[:, 0]

    sqrt_logits = np.sign(logits) * np.sqrt(np.abs(logits))

    ttV_mask = np.all([procid >= 7000, procid < 8000], axis=0)
    rare_mask = np.all([procid >= 20000, procid < 21000], axis=0)
    ggH_VBF_mask = np.all([procid >= 9000, procid < 9300], axis=0)
    ttH_mask = np.all([procid >= 9600, procid < 9950], axis=0)

    bg_processes = {
        "TT": np.all([procid >= 2000, procid < 3000], axis=0),
        "DY": np.all([procid >= 3000, procid < 3500], axis=0),
        "VV(V)": np.all([procid >= 4000, procid < 5000], axis=0),
        "ST": np.all([procid >= 5000, procid < 6000], axis=0),
        "W + Jets": np.all([procid >= 6000, procid < 7000], axis=0),
        "QCD": np.all([procid >= 8000, procid < 9000], axis=0),
        "Other": np.any([ttV_mask, rare_mask, ggH_VBF_mask, ttH_mask], axis=0),
    }
    sig_processes = {
        "ggZH": (procid == 9421),
        "ZH DY": (procid == 9411),
        "WH": np.any([procid == 9502, procid == 9504], axis=0),
    }
    processes = {**bg_processes, **sig_processes}

    # weights for clustering
    total_weight = np.sum(weight)
    clustering_weight = np.full_like(weight, -1)
    for process, mask in processes.items():
        process_weight = weight[mask].sum()
        np.place(clustering_weight, mask, weight[mask] / process_weight * total_weight)

    """
    kmeans clustering
    """
    tic_clustering = time.time()
    n_cluster = 80
    print(f"Number of clusters: {n_cluster}")
    print("Start clustering")
    kmeans = KMeans(
        n_clusters=n_cluster,
        max_iter=300,
        init="random",
        algorithm="full",
        random_state=0,
        tol=1e-5,
        # verbose=1,
    ).fit(sqrt_logits, sample_weight=clustering_weight)
    print("Finished clustering")

    toc_clustering = time.time()
    with open(
        output_path + f"{n_cluster}_cluster_centers_sqrt.json",
        "w",
    ) as outfile:
        json.dump(kmeans.cluster_centers_.tolist(), outfile)

    # cluster = kmeans.predict(logits)
    pred = kmeans.labels_
    sorted_pred = sort_bins(pred, weight, n_cluster, sig_processes)

    """
    plotting
    """
    from plotting import cluster_histogram, plot_cluster

    cluster_histogram(pred, procid, n_cluster, outfile=f"{output_path}cluster_sqrt")
    plot_cluster(
        sorted_pred,
        weight,
        bg_processes,
        sig_processes,
        # colors,
        n_cluster,
        outfile=f"{output_path}{n_cluster}_hist_sorted_sqrt",
    )

    """
    limits
    """
    tic_limit = time.time()
    from fitting import limits

    signals, backgrounds = binning_from_clusters(
        pred, weight, bg_processes, sig_processes, n_cluster
    )

    exp_limits = limits(signals, backgrounds)

    print(f"Expected limit is: {exp_limits[2]}")
    print(f"1 {chr(963)} interval: {exp_limits[1]} - {exp_limits[3]}")
    print(f"2 {chr(963)} interval: {exp_limits[0]} - {exp_limits[4]}")
    toc_limit = time.time()

    # visualise  data, takes super long!!!
    # from plotting import TSNE_visualisation2D
    # TSNE_visualisation2D(
    #     logits,
    #     kmeans.labels_,
    #     outfile=f"{output_path}TSNE_2D_algo-full_random",
    #     lr=1000,
    #     v=1,
    # )

    toc = time.time()
    total_time = (toc - tic) / 60
    clustering_time = (toc_clustering - tic_clustering) / 60
    limit_time = (toc_limit - tic_limit) / 60

    print(f"Total time: {total_time:.2f}min")
    print(f"Clustering time: {clustering_time:.2f}min")
    print(f"Fitting time: {limit_time:.2f}min")
