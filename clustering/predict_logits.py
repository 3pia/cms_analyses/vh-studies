import argparse
import os
import numpy as np

from sklearn.cluster import KMeans
from plotting import plot_cluster
from run_clustering import load_data, preprocess_logits, split_processes, process_weights

def main(input_path, output_path, centers, n_clusters=50, categories=["mumu_2b"], split_signal=False):

    logits, procid, weights = load_data(input_path, categories)
    logits = preprocess_logits(logits)

    signal_processes, background_processes = split_processes(procid, split_signal=split_signal)
    processes = {**background_processes, **signal_processes}
    clustering_weights = process_weights(weights, processes)

    centers = np.load(centers)
    kmeans = KMeans(n_clusters=n_clusters, init=centers)
    kmeans.fit(centers) # needs to be done to init the object

    predictions = kmeans.predict( logits )

    # cluster_histogram(cluster_labels, procid, n_clusters, os.path.join(output_path, "{0:03d}_cluster_sqrt".format(n_clusters)))
    plot_cluster(
        predictions,
        weights,
        background_processes,
        signal_processes,
        n_clusters,
        outfile=os.path.join(output_path, "{0:03d}_hist_sorted_{1}_sqrt".format(n_clusters, "_".join(categories)))
    )


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Clustering algorithm')
    parser.add_argument('input_path', metavar='i', type=str,
                        help='input path of LogitExporterProcessor')
    parser.add_argument('centers', metavar='c', type=str,
                        help='input path of Centers')
    parser.add_argument('output_path', metavar='o', type=str,
                        help='output path of Clustering')
    parser.add_argument('--n-clusters', default=50, type=int, help='Integer number of clusters to use')
    parser.add_argument('--categories', default=["mumu_2b"], nargs='*', help='Categories to use')
    parser.add_argument('--split-signal', default=False, action="store_true", help='Categories to use')
    parser.add_argument('--verbose', default=1, type=int, help="Verbosity level")

    args = parser.parse_args()
    # print(args)
    os.makedirs(args.output_path, exist_ok=True)

    main(args.input_path, args.output_path, args.centers, n_clusters=args.n_clusters, categories=args.categories, split_signal=args.split_signal)