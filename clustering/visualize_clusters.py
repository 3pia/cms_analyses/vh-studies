import numpy as np
import matplotlib.pyplot as plt
from plotting import colors as proc_colors
from matplotlib.pyplot import cm
import mplhep

input_path = (
    "/net/scratch/cms/dihiggs/store/vh/vh-studies/clustering/output/UL_prod01_softmax_dpg1/"
)
cluster_centers = np.load(f"{input_path}200_cluster_centers.npy")

print(cluster_centers.shape)

N_clusters = 18
colors = cm.rainbow(np.linspace(0, 1, N_clusters))
labels = ["VH", "TT", "DY", "VV(V)", "ST", "W + Jets", "QCD", "Other"]
n_columns = 6
n_rows = (N_clusters + n_columns - 1) // n_columns


theta = np.linspace(0, 2 * np.pi, cluster_centers.shape[-1], endpoint=False)


x = np.arange(n_rows * n_columns).reshape(n_rows, n_columns)

fig, ax = plt.subplots(
    n_rows,
    n_columns,
    subplot_kw={"projection": "polar"},
    figsize=(n_columns * 6.0, 7.0 * n_rows),
)
fig.tight_layout(pad=5.0)
rmax = np.max(abs(cluster_centers[-N_clusters:])) * 1.1
for i in range(n_rows):
    for j in range(n_columns):
        idx = x[i, j]
        if N_clusters - idx <= 0:
            fig.delaxes(ax[i][j])
            continue
        else:
            print(f"Cluster {idx}")
            print(cluster_centers[-idx])
            cci = cluster_centers[-idx]

            size = (cci) ** 2 * 1000 * 2
            positives = cci >= 0
            negatives = np.invert(positives)
            ax[i][j].scatter(
                theta[positives],
                abs(cci[positives]),
                s=size[positives],
                color=colors[idx],
                marker="o",
            )
            ax[i][j].scatter(
                theta[negatives],
                abs(cci[negatives]),
                s=size[negatives],
                color=colors[idx],
                marker="*",
            )
            ax[i][j].set_xticks(theta)
            ax[i][j].set_rmax(rmax)
            ax[i][j].set_rmax(1.0)
            ax[i][j].set_xticklabels(labels, fontsize=20)
            ax[i][j].set_theta_zero_location("N")
            ax[i][j].set_theta_direction(-1)
            ax[i][j].set_title(f"Cluster #{idx}", fontsize=25, loc="left")
fig.savefig(f"{input_path}clusters_grid_score.png")


theta = np.linspace(0, 2 * np.pi, cluster_centers.shape[-1], endpoint=False)
fig, ax = plt.subplots(1, 1, subplot_kw={"projection": "polar"}, figsize=(20, 20))
fig.tight_layout(pad=5.0)
for idx in range(N_clusters):
    cci = cluster_centers[-idx]
    size = (cci) ** 2 * 1000 * 6
    positives = cci >= 0
    negatives = np.invert(positives)
    ax.scatter(
        theta[positives],
        abs(cci[positives]),
        s=size[positives],
        color=colors[idx],
        marker="o",
    )
    ax.scatter(
        theta[negatives],
        abs(cci[negatives]),
        s=size[negatives],
        color=colors[idx],
        marker="*",
    )
ax.set_xticks(theta)
ax.set_rmax(np.max(abs(cluster_centers[-N_clusters:])) * 1.05)
ax.set_xticklabels(labels, fontsize=20)
ax.set_theta_zero_location("N")
ax.set_theta_direction(-1)
ax.set_title(f"Top Clusters: {N_clusters}", fontsize=25, loc="left")
fig.savefig(f"{input_path}clusters_all_score.png")


from matplotlib.projections.polar import PolarAxes
from matplotlib.projections import register_projection
from matplotlib.spines import Spine
from matplotlib.transforms import Affine2D
import matplotlib.pyplot as plt
from matplotlib.patches import Circle, RegularPolygon
from matplotlib.path import Path

>>>>>>> 6d693834596113f35f87cef66cc71be3743a1ede

def radar_factory(num_vars, frame="circle"):
    """
    Create a radar chart with `num_vars` axes.

    This function creates a RadarAxes projection and registers it.

    Parameters
    ----------
    num_vars : int
        Number of variables for radar chart.
    frame : {'circle', 'polygon'}
        Shape of frame surrounding axes.

    """

    class RadarTransform(PolarAxes.PolarTransform):
        def transform_path_non_affine(self, path):
            # Paths with non-unit interpolation steps correspond to gridlines,
            # in which case we force interpolation (to defeat PolarTransform's
            # autoconversion to circular arcs).
            if path._interpolation_steps > 1:
                path = path.interpolated(num_vars)
            return Path(self.transform(path.vertices), path.codes)

    class RadarAxes(PolarAxes):

        name = "radar"
        PolarTransform = RadarTransform

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            # rotate plot such that the first axis is at the top
            self.set_theta_zero_location("N")

        def fill(self, *args, closed=True, **kwargs):
            """Override fill so that line is closed by default"""
            return super().fill(closed=closed, *args, **kwargs)

        def plot(self, *args, **kwargs):
            """Override plot so that line is closed by default"""
            lines = super().plot(*args, **kwargs)
            for line in lines:
                self._close_line(line)

        def _close_line(self, line):
            x, y = line.get_data()
            # FIXME: markers at x[0], y[0] get doubled-up
            if x[0] != x[-1]:
                x = np.append(x, x[0])
                y = np.append(y, y[0])
                line.set_data(x, y)

        def set_varlabels(self, labels):
            self.set_thetagrids(np.degrees(theta), labels)

        def _gen_axes_patch(self):
            # The Axes patch must be centered at (0.5, 0.5) and of radius 0.5
            # in axes coordinates.
            if frame == "circle":
                return Circle((0.5, 0.5), 0.5)
            elif frame == "polygon":
                return RegularPolygon((0.5, 0.5), num_vars, radius=0.5, edgecolor="k")
            else:
                raise ValueError("Unknown value for 'frame': %s" % frame)

        def _gen_axes_spines(self):
            if frame == "circle":
                return super()._gen_axes_spines()
            elif frame == "polygon":
                # spine_type must be 'left'/'right'/'top'/'bottom'/'circle'.
                spine = Spine(
                    axes=self,
                    spine_type="circle",
                    path=Path.unit_regular_polygon(num_vars),
                )
                # unit_regular_polygon gives a polygon of radius 1 centered at
                # (0, 0) but we want a polygon of radius 0.5 centered at (0.5,
                # 0.5) in axes coordinates.
                spine.set_transform(Affine2D().scale(0.5).translate(0.5, 0.5) + self.transAxes)
                return {"polar": spine}
            else:
                raise ValueError("Unknown value for 'frame': %s" % frame)

    # calculate evenly-spaced axis angles
    theta = np.linspace(0, 2 * np.pi, num_vars, endpoint=False)
    register_projection(RadarAxes)
    return theta


theta = radar_factory(cluster_centers.shape[-1], frame="polygon")

for idx in range(10):
    fig, ax = plt.subplots(1, 1, subplot_kw={"projection": "radar"}, figsize=(20, 20))
    # fig, ax = plt.subplots(figsize=(9, 9), nrows=1, ncols=1,
    #                             subplot_kw=dict(projection='radar'))

    ax.set_rmax(0.8)  # np.max(abs(cluster_centers[-N_clusters:])) * 1.05)
    ax.set_rmin(0.0)
    # ax.set_rticks([0.2, 0.4, 0.6, 0.8], size=30)
    ax.set_rgrids(
        # [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8],
        # [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8],
        [0.2, 0.4, 0.6, 0.8],
        [0.2, 0.4, 0.6, 0.8],
        size=40,
    )
    cci = cluster_centers[-idx]
    size = (cci) ** 2 * 1000 * 6
    positives = cci >= 0
    negatives = np.invert(positives)
    ax.plot(theta, cci, color=colors[3], marker="o")
    ax.fill(theta, cci, facecolor=colors[3], alpha=0.2)
    ax.set_xticks(theta)
    ax.set_xticklabels(labels, fontsize=45)
    ax.set_rmax(0.8)  # np.max(abs(cluster_centers[-N_clusters:])) * 1.05)
    ax.set_rmin(0.0)
    # ax.set_rticks([0.2, 0.4, 0.6, 0.8], size=30)
    ax.set_rgrids(
        # [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8],
        # [0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8],
        [0.0, 0.2, 0.4, 0.6, 0.8],
        [0.0, 0.2, 0.4, 0.6, 0.8],
        size=35,
    )

    for label, angle in zip(ax.get_xticklabels(), theta):
        if angle in (0, np.pi):
            label.set_horizontalalignment("center")
        elif 0 < angle < np.pi:
            label.set_horizontalalignment("left")
        else:
            label.set_horizontalalignment("right")
    ax.tick_params(axis="x", which="major", pad=15)
    # ax.tick_params(axis="y", which="major", pad=40)
    ax.set_theta_zero_location("N")
    ax.set_theta_direction(-1)
    ax.set_title("Cluster #{0:2d}".format(idx + 1), fontsize=50, loc="right")  # , y=-0.01
    mplhep.cms.label(label="Preliminary", loc=0, year=2017, fontsize=40, ax=ax, pad=0.05, rlabel="")
    fig.tight_layout(pad=5.0)
    fig.savefig(f"{input_path}plots/clusters_all_score_radar_{idx:02d}.png")
