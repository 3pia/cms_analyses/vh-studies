import argparse
import json
import logging
import os
import time
import numpy as np
from scipy.special import softmax

from fitting import calculate_limits_from_clusters
from plotting import (
    cluster_histogram,
    plot_cluster,
    histogram_n_events,
    visualisation2D,
)
from clustering import clustering

from utils import load_data, preprocess_logits, split_processes, preprocess_weights, sort_bins, sort_by_keym, sort_clusters

log = logging.getLogger(__name__)

MAXEVENTS = -1


def main(
    input_path,
    output_path,
    n_clusters=50,
    categories=["mumu_2b"],
    max_iter=100,
    split_signal=False,
    mini_batch=True,
    batch_size=1024,
    no_limit=False,
    use_softmax=False,
    embedding=False,
    tag="",
):
    """
    Preprocessing
    """
    logits, procid, weights = load_data(input_path, categories, variable=kwargs.pop("variable", "logit"))

    logits = preprocess_logits(logits, use_softmax)
    signal_processes, background_processes = split_processes(procid, split_signal=split_signal)
    processes = {**background_processes, **signal_processes}
    clustering_weights = process_weights(weights, processes)

    """
    Clustering
    """
    clustering_result = clustering(
        logits,
        procid,
        clustering_weights,
        n_clusters,
        max_iter=max_iter,
        MiniBatch=mini_batch,
        batch_size=batch_size,
    )
    cluster_labels = clustering_result.labels_
    sorted_labels = sort_bins(cluster_labels, weights, n_clusters, signal_processes)

    cluster_centers = clustering_result.cluster_centers_
    cluster_centers_sorted = sort_clusters(
        cluster_centers, cluster_labels, weights, n_clusters, signal_processes
    )

    np.save(
        os.path.join(output_path, "{0:03d}_{1}cluster_centers.npy".format(n_clusters, tag)),
        cluster_centers_sorted,
    )

    """
    Plotting
    """
    histogram_n_events(
        sorted_labels,
        os.path.join(output_path, "{0:03d}_{1}cluster_occupations.png".format(n_clusters, tag)),
        max_bins=n_clusters // 4,
    )
    # make plots for different sortings
    proc_keys = ["VH"]  # , "DY", "TT"]
    for proc_key in proc_keys:
        if not isinstance(proc_key, list):
            proc_key = [proc_key]
        # cluster_histogram(cluster_labels, procid, n_clusters, os.path.join(output_path, "{0:03d}_cluster_sqrt".format(n_clusters)))
        sorted_labels = sort_by_key(
            cluster_labels,
            weights,
            n_clusters,
            proc_key,
            signal_processes,
            background_processes,
        )
        plot_cluster(
            sorted_labels,
            weights,
            background_processes,
            signal_processes,
            n_clusters,
            outfile=os.path.join(
                output_path,
                "{0:03d}_{1}hist_sorted_{2}".format(n_clusters, tag, "_".join(proc_key)),
            ),
        )

    if not (no_limit) is True:
        signal_processes_fit, background_processes_fit = split_processes_fit(
            procid, split_signal=False
        )
        exp_limits = calculate_limits_from_clusters(
            cluster_labels,
            weights,
            background_processes_fit,
            signal_processes_fit,
            n_clusters,
        )
        with open(
            os.path.join(output_path, "{0:03d}_{1}limits.json".format(n_clusters, tag)),
            "w",
        ) as limits_file:
            out = {
                "n_clusters": n_clusters,
                "max_iter": max_iter,
                "exp_limit": list(map(float, exp_limits)),
            }
            json.dump(out, limits_file)

    if embedding:
        try:
            x_embed = np.load(output_path + f"/PCA.npy")
            visualisation2D(
                x_embed,
                sorted_labels,
                n_components=2,
                outfile=os.path.join(output_path, "pca_embedding"),
            )

        except FileNotFoundError as e:
            log.error(f"File {output_path} PCA.npy was not found! Skipping")
            raise


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description="Clustering algorithm")
    parser.add_argument(
        "input_path", metavar="i", type=str, help="input path of LogitExporterProcessor"
    )
    parser.add_argument("output_path", metavar="o", type=str, help="output path of Clustering")
    parser.add_argument(
        "--batch-size",
        default=1024,
        type=int,
        help="Batch size for minibatch clustering",
    )
    parser.add_argument("--categories", default=["mumu_2b"], nargs="*", help="Categories to use")
    parser.add_argument("--max-iter", default=100, type=int, help="Maximum iteration of clustering")
    parser.add_argument(
        "--mini-batch",
        default=False,
        action="store_true",
        help="Use mini-batch clustering",
    )
    parser.add_argument(
        "--n-clusters", default=50, type=int, help="Integer number of clusters to use"
    )
    parser.add_argument("--no-limit", default=False, action="store_true", help="Do not make fit")
    parser.add_argument(
        "--split-signal", default=False, action="store_true", help="Categories to use"
    )
    parser.add_argument(
        "--use-softmax",
        default=False,
        action="store_true",
        help="use softmax on logits before clustering",
    )
    parser.add_argument(
        "--embedding",
        default=False,
        action="store_true",
        help="plot embedding",
    )
    parser.add_argument("--tag", default="", type=str, help="Tag to save the plot to")
    parser.add_argument("--verbose", default=1, type=int, help="Verbosity level")

    args = parser.parse_args()
    # print(args)
    os.makedirs(args.output_path, exist_ok=True)

    FORMAT = "Clustering: %(levelname)s %(message)s"
    logging.basicConfig(
        level=10 if args.verbose == 2 else 20 if args.verbose == 1 else 30,
        format=FORMAT,
    )

    main(
        args.input_path,
        args.output_path,
        n_clusters=args.n_clusters,
        categories=args.categories,
        max_iter=args.max_iter,
        split_signal=args.split_signal,
        mini_batch=args.mini_batch,
        batch_size=args.batch_size,
        no_limit=args.no_limit,
        use_softmax=args.use_softmax,
        embedding=args.embedding,
        tag=args.tag,
    )