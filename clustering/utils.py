import argparse
import json
import logging
import os
import time
import numpy as np

from scipy.special import softmax
from plotting import cluster_histogram, plot_cluster, histogram_n_events
from clustering import clustering

log = logging.getLogger(__name__)
MAXEVENTS = -1


def load_data(input_path, categories, verbose=True):
    log.info("Reading inputs")
    logits, procid, weight = [], [], []
    for category in categories:
        try:
            logits.append(np.load(input_path + f"{category}_logit.npy")[:MAXEVENTS])
            procid.append(np.load(input_path + f"{category}_procid.npy")[:MAXEVENTS].flatten())
            weight.append(np.load(input_path + f"{category}_weight.npy")[:MAXEVENTS])
        except FileNotFoundError as e:
            log.error(f"File {input_path+ category} was not found! Skipping")
    try:
        logits = np.concatenate(logits, axis=0)
        procid = np.concatenate(procid, axis=0)
        weight = np.concatenate(weight, axis=0)
    except ValueError as e:
        logits = np.zeros(100)
        procid = np.zeros(100)
        weight = np.zeros(100)
        log.error("Exception during array concatenation\n:", e)
        log.error("Proceeding with empty logits")
        raise
    log.info("Finished reading inputs")
    log.info("Shape of logits: {}".format(logits.shape))
    return logits, procid, weight


def preprocess_logits(logits, use_softmax=False):
    log.info("Preprocessing logits")
    sqrt_logits = np.sign(logits) * np.sqrt(np.abs(logits))
    scores = softmax(logits, axis=1)
    return scores if use_softmax else sqrt_logits


def split_processes(procid, split_signal=True):
    log.info("Splitting processes into signal and background")

    if split_signal:
        sig_processes = {
            "ggZH": (procid == 9421),
            "ZH DY": (procid == 9411),
            "WH": np.any([procid == 9502, procid == 9504], axis=0),
        }
    else:
        sig_processes = {
            "VH": np.any([procid == 9421, procid == 9502, procid == 9411, procid == 9504], axis=0),
        }

    ttV_mask = np.all([procid >= 7000, procid < 8000], axis=0)
    rare_mask = np.all([procid >= 20000, procid < 21000], axis=0)
    ggH_VBF_mask = np.all([procid >= 9000, procid < 9300], axis=0)
    ttH_mask = np.all([procid >= 9600, procid < 9950], axis=0)

    bg_processes = {
        "TT": np.all([procid >= 2000, procid < 3000], axis=0),
        "DY": np.all([procid >= 3000, procid < 3500], axis=0),
        "VV(V)": np.all([procid >= 4000, procid < 5000], axis=0),
        "ST": np.all([procid >= 5000, procid < 6000], axis=0),
        "W + Jets": np.all([procid >= 6000, procid < 7000], axis=0),
        "QCD": np.all([procid >= 8000, procid < 9000], axis=0),
        "Other": np.any([ttV_mask, rare_mask, ggH_VBF_mask, ttH_mask], axis=0),
    }
    return sig_processes, bg_processes


def split_processes_fit(procid, split_signal=False):
    log.info("Splitting processes into signal and background for fitting")

    if split_signal:
        sig_processes = {
            "ggZH": (procid == 9421),
            "ZH DY": (procid == 9411),
            "WH": np.any([procid == 9502, procid == 9504], axis=0),
        }
    else:
        sig_processes = {
            "VH": np.any([procid == 9421, procid == 9502, procid == 9411, procid == 9504], axis=0),
        }

    # ttV_mask = np.all([procid >= 7000, procid < 8000], axis=0)
    # rare_mask = np.all([procid >= 20000, procid < 21000], axis=0)
    # ggH_VBF_mask = np.all([procid >= 9000, procid < 9300], axis=0)
    # ttH_mask = np.all([procid >= 9600, procid < 9950], axis=0)

    bg_processes = {
        "VBFH": np.all([procid >= 9200, procid < 9250], axis=0),
        "ggH": np.all([procid >= 9100, procid < 9150], axis=0),
        "wjets": np.all([procid >= 6000, procid < 7000], axis=0),
        "dy": np.all([procid >= 3000, procid < 3500], axis=0),
        "vv": np.all([procid >= 4000, procid < 4550], axis=0),
        "vvv": np.all([procid >= 4600, procid < 4650], axis=0),
        "st": np.all([procid >= 5000, procid < 6000], axis=0),
        "tt": np.all([procid >= 2000, procid < 3000], axis=0),
        "ttV": np.all([procid >= 7000, procid < 7250], axis=0),
        "ttH": np.all([procid >= 9600, procid < 9650], axis=0),
        "ttVH": np.all([procid >= 9900, procid < 9950], axis=0),
        "ttVV": np.all([procid >= 7300, procid < 7350], axis=0),
        "QCD": np.all([procid >= 8000, procid < 9000], axis=0),
        # "VV(V)": np.all([procid >= 4000, procid < 5000], axis=0),
        # "Other": np.any([ttV_mask, rare_mask, ggH_VBF_mask, ttH_mask], axis=0),
    }
    return sig_processes, bg_processes


def process_weights(weights, processes):
    log.info("Processing weights")
    total_weight = np.sum(weights)
    clustering_weight = np.full_like(weights, -1)
    for process, mask in processes.items():
        process_weight = weights[mask].sum()
        np.place(clustering_weight, mask, weights[mask] / process_weight * total_weight)
    return clustering_weight


def sort_bins(pred, weight, n_cluster, sig_processes):
    signal_mask = np.any([mask for mask in sig_processes.values()], axis=0)
    sig_pred = pred[signal_mask]
    sig_weight = weight[signal_mask]
    sig_weight_sum = [sig_weight[sig_pred == i_cluster].sum() for i_cluster in range(n_cluster)]

    sorting = np.argsort(sig_weight_sum)
    sorted_pred = np.full_like(pred, -1)
    for i in range(n_cluster):
        np.place(sorted_pred, pred == sorting[i], i)
    return sorted_pred


def sort_by_key(pred, weight, n_cluster, keys, signal_processes, background_processes):
    if not isinstance(keys, list):
        keys = [keys]

    all_procs = signal_processes.copy()
    all_procs.update(background_processes)

    mask = np.any([all_procs[key] for key in keys], axis=0)
    proc_pred = pred[mask]
    proc_weight = weight[mask]
    proc_weight_sum = [proc_weight[proc_pred == i_cluster].sum() for i_cluster in range(n_cluster)]

    sorting = np.argsort(proc_weight_sum)
    sorted_pred = np.full_like(pred, -1)
    for i in range(n_cluster):
        np.place(sorted_pred, pred == sorting[i], i)
    return sorted_pred


def sort_clusters(clusters, pred, weight, n_cluster, sig_processes):
    signal_mask = np.any([mask for mask in sig_processes.values()], axis=0)
    sig_pred = pred[signal_mask]
    sig_weight = weight[signal_mask]
    sig_weight_sum = [sig_weight[sig_pred == i_cluster].sum() for i_cluster in range(n_cluster)]

    sorting = np.argsort(sig_weight_sum)
    return clusters[sorting]