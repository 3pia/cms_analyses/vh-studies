import os
import tensorflow as tf
import numpy as np
import xgboost as xgb
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier, GradientBoostingClassifier
from sklearn.metrics import roc_curve, roc_auc_score
from plotting import plot_roc, plot_metric, plot_hist
import argparse

parser = argparse.ArgumentParser()

parser.add_argument("-v", "--version", default=0, help="Verison to save file, i.e. number", type=str)
args = parser.parse_args()

out_dir = "plots/ggDY_tree_{}/".format(args.version)
os.makedirs(out_dir, exist_ok=True)

variables = ["jet", "hl", "lep"]
"""
"""

data_signal = []
data_background = []
try:
    for var in variables:
        print("Reading in file {}".format(var))
        data_signal.append(np.load(os.path.join(os.getenv("DATA_PATH"), "ZH_bbll+ee_2b+{}.npy").format(var)))
        data_background.append(np.load(os.path.join(os.getenv("DATA_PATH"), "ggZH_bbll+ee_2b+{}.npy").format(var)))
except FileNotFoundError:
    print("Your file was not found!")
    raise Exception

data_signal = np.concatenate( ([d.reshape(d.shape[0], -1) for d in data_signal]), axis=-1)
data_background = np.concatenate( ([d.reshape(d.shape[0], -1) for d in data_background]), axis=-1)

print("Events:")
print("ZH:\t{0:7d}".format(len(data_signal)))
print("ggZH:\t{0:7d}".format(len(data_background)))

labels_signal = np.zeros((data_signal.shape[0], 2))
labels_signal[..., 0] = 1
labels_back = np.zeros((data_background.shape[0], 2))
labels_back[..., 1] = 1

data = np.concatenate( (data_signal, data_background), axis=0)
labels = np.concatenate( (labels_signal, labels_back), axis=0)

X_train, X_test, y_train, y_test = train_test_split(data, labels, test_size=0.33, random_state=42)

X_val, X_test, y_val, y_test = train_test_split(X_test, y_test, test_size=0.5, random_state=42)



# Preprocessing
data = (data-np.mean(data))/np.std(data)
# class weights

class_weight = "balanced" 


# build model

L2 = 1e-3

# tree = DecisionTreeClassifier(class_weight=class_weight)
# tree = RandomForestClassifier(class_weight=class_weight)
tree = xgb.XGBClassifier(n_estimators=100,
                         max_depth=13,
                         gamma=3,
                         verbosity=1)
# tree = GradientBoostingClassifier(
            # n_estimators=500,
            # verbose=1,
            # learning_rate=0.1)

summary = tree.fit(X_train, np.argmax(y_train, axis=-1)) 

"""
Evaluation

"""
y_pred = tree.predict(X_train)
y_pred_ggZH = y_pred[np.where(np.argmax(y_train, axis=-1) == 1)]
y_pred_ZH = y_pred[np.where(np.argmax(y_train, axis=-1) == 0)]

true_pos = len(np.where( y_pred_ggZH == 1)[0])
true_neg = len(np.where( y_pred_ZH == 0)[0])
false_pos = len(np.where( y_pred_ZH == 1)[0])
false_neg = len(np.where( y_pred_ggZH == 0 )[0])

# Histogram of output
plot_hist( [y_pred_ZH, y_pred_ggZH], ["DY-ZH", "gg-ZH"], os.path.join(out_dir, "output_hist_train.png"), bins=20, xlabel="BDT-output", ylabel="Events (normed)")

print("#### ggZH ####")
print("Train:")
# precision: true positive / ( true positive + false positive)
# print("Precision:\t{0:1.2f}".format(true_pos/(true_pos + false_pos)))
print("Recall:\t{0:1.2f}".format(true_pos/(true_pos + false_neg)))
print("True Positive vs False Positive:\t{} vs {}".format(true_pos, false_pos))
print("True Negative vs False Negative:\t{} vs {}".format(true_neg, false_neg))
# Test-Set
y_pred = tree.predict(X_test)
y_pred_ZH = y_pred[np.where(np.argmax(y_test, axis=-1) == 0)]
y_pred_ggZH = y_pred[np.where(np.argmax(y_test, axis=-1) == 1)]

true_pos = len(np.where( y_pred_ggZH == 1)[0])
true_neg = len(np.where( y_pred_ZH == 0)[0])
false_pos = len(np.where( y_pred_ZH == 1)[0])
false_neg = len(np.where( y_pred_ggZH == 0 )[0])

print("#"*20)
print("Test:")
# precision: true positive / ( true positive + false positive)
# print("Precision:\t{0:1.2f}".format(true_pos/(true_pos + false_pos)))
print("Recall:\t{0:1.2f}".format(true_pos/(true_pos + false_neg)))
print("True Positive vs False Positive:\t{} vs {}".format(true_pos, false_pos))
print("True Negative vs False Negative:\t{} vs {}".format(true_neg, false_neg))

# ROC-curves
# fpr_keras, tpr_keras, thresholds_keras = roc_curve(y_test, y_pred )
# auc_test = roc_auc_score(y_test, y_pred )
# plot_roc(fpr_keras, tpr_keras, out_dir, auc=auc_test)

# Histogram of output
plot_hist( [y_pred_ZH, y_pred_ggZH], ["DY-ZH", "gg-ZH"], os.path.join(out_dir, "output_hist_test.png"), bins=20, xlabel="BDT-output", ylabel="Events (normed)")
