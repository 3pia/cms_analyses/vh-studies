import os
import tensorflow as tf
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import roc_curve, roc_auc_score
from plotting import plot_roc, plot_metric, plot_hist
import argparse

parser = argparse.ArgumentParser()

parser.add_argument("-v", "--version", default=0, help="Verison to save file, i.e. number", type=str)
args = parser.parse_args()

out_dir = "plots/ggDY_{}/".format(args.version)
os.makedirs(out_dir, exist_ok=True)

variables = ["jet", "hl", "lep"]
# variables = ["jet"]
"""
"""

data_signal = []
data_background = []
try:
    for var in variables:
        print("Reading in file {}".format(var))
        data_signal.append(np.load(os.path.join(os.getenv("DATA_PATH"), "ZH_bbll+ee_2b+{}.npy").format(var)))
        data_background.append(np.load(os.path.join(os.getenv("DATA_PATH"), "ggZH_bbll+ee_2b+{}.npy").format(var)))
except FileNotFoundError:
    print("Your file was not found!")
    raise Exception

data_signal = np.concatenate( ([d.reshape(d.shape[0], -1) for d in data_signal]), axis=-1)
data_background = np.concatenate( ([d.reshape(d.shape[0], -1) for d in data_background]), axis=-1)

print("Events:")
print("ZH:\t{}".format(len(data_signal)))
print("ggZH:\t{}".format(len(data_background)))

labels_signal = np.zeros((data_signal.shape[0], 2))
labels_signal[..., 0] = 1
labels_back = np.zeros((data_background.shape[0], 2))
labels_back[..., 1] = 1

data = np.concatenate( (data_signal, data_background), axis=0)
labels = np.concatenate( (labels_signal, labels_back), axis=0)

shuffle = np.random.permutation(len(data))

data = data[shuffle]
labels = labels[shuffle]

X_train, X_test, y_train, y_test = train_test_split(data, labels, test_size=0.33, random_state=42)
X_val, X_test, y_val, y_test = train_test_split(X_test, y_test, test_size=0.5, random_state=42)



# Preprocessing
data = (data-np.mean(data))/np.std(data)
# class weights
weight_for_0 = (1 / len(labels_signal))*(len(labels_signal) + len(labels_signal))/2.0 
weight_for_1 = (1 / len(labels_back))*(len(labels_back) + len(labels_signal))/2.0 

class_weight = {0: weight_for_0, 1: weight_for_1}
print("Weight for class 0 (ZH):\t{}".format(weight_for_0))
print("Weight for class 1 (ggZH):\t{}".format(weight_for_1))

# build model

L2 = 1e-3

model = tf.keras.Sequential([
    tf.keras.layers.Flatten(input_shape=data.shape[1:]),
    tf.keras.layers.Dense(200,kernel_regularizer=tf.keras.regularizers.l2(L2), activation='relu'),
    tf.keras.layers.Dropout(0.1),
    tf.keras.layers.Dense(200,kernel_regularizer=tf.keras.regularizers.l2(L2), activation='relu'),
    tf.keras.layers.Dropout(0.1),
    tf.keras.layers.Dense(200,kernel_regularizer=tf.keras.regularizers.l2(L2), activation='relu'),
    tf.keras.layers.Dropout(0.1),
    tf.keras.layers.Dense(2, activation='softmax')
])

model.compile(optimizer='adam',
              loss=tf.keras.losses.CategoricalCrossentropy(from_logits=False),
              metrics=[tf.keras.metrics.BinaryAccuracy(name='accuracy'),
                       tf.keras.metrics.AUC(name='auc')],
              )

print(model.summary())

lr_callback = tf.keras.callbacks.ReduceLROnPlateau(
        monitor='val_loss', factor=0.5, patience=70, verbose=1,
        mode='auto', min_delta=0.0001, cooldown=70, min_lr=0
)

summary = model.fit(X_train, y_train, epochs=2000, batch_size=128,
              validation_data=(X_val, y_val),
              class_weight=class_weight,
              callbacks=[lr_callback],
              verbose=1
        ) 
history = summary.history
"""
Evaluation

"""
# Training-Set
y_pred = model.predict(X_train)
y_pred_ggZH = y_pred[np.where(np.argmax(y_train, axis=-1) == 1)[0], 1]
y_pred_ZH = y_pred[np.where(np.argmax(y_train, axis=-1) == 0)[0], 0]

true_pos = len(np.where( y_pred_ggZH > 0.5)[0])
true_neg = len(np.where( y_pred_ZH > 0.5)[0])
false_pos = len(np.where( y_pred_ZH < 0.5)[0])
false_neg = len(np.where( y_pred_ggZH < 0.5)[0])

print("#### ggZH ####")
print("Train:")
# precision: true positive / ( true positive + false positive)
# print("Precision:\t{0:1.2f}".format(true_pos/(true_pos + false_pos)))
print("Recall:\t{0:1.2f}".format(true_pos/(true_pos + false_neg)))
print("True Positive vs False Positive:\t{} vs {}".format(true_pos, false_pos))
print("True Negative vs False Negative:\t{} vs {}".format(true_neg, false_neg))
# Test-Set
y_pred = model.predict(X_test)
y_pred_ggZH = y_pred[np.where(np.argmax(y_test, axis=-1) == 1)[0], 1]
y_pred_ZH = y_pred[np.where(np.argmax(y_test, axis=-1) == 0)[0], 0]

true_pos = len(np.where( y_pred_ggZH > 0.5)[0])
true_neg = len(np.where( y_pred_ZH > 0.5)[0])
false_pos = len(np.where( y_pred_ZH < 0.5)[0])
false_neg = len(np.where( y_pred_ggZH < 0.5)[0])

print("#"*20)
print("Test:")
# precision: true positive / ( true positive + false positive)
# print("Precision:\t{0:1.2f}".format(true_pos/(true_pos + false_pos)))
print("Recall:\t{0:1.2f}".format(true_pos/(true_pos + false_neg)))
print("True Positive vs False Positive:\t{} vs {}".format(true_pos, false_pos))
print("True Negative vs False Negative:\t{} vs {}".format(true_neg, false_neg))

evaluation = model.evaluate(X_test, y_test)

plot_metric(history["loss"], history["val_loss"], "loss", out_dir, log=True)
plot_metric(history["auc"], history["val_auc"], "auc", out_dir)
plot_metric(history["accuracy"], history["val_accuracy"], "accuracy", out_dir)

# ROC-curves
fpr_keras, tpr_keras, thresholds_keras = roc_curve(np.argmax(y_test, axis=-1), y_pred[..., 1] )
auc_test = roc_auc_score(np.argmax(y_test, axis=-1), y_pred[..., 1] )
plot_roc(fpr_keras, tpr_keras, out_dir, auc=auc_test)

# Histogram of output
plot_hist( [y_pred_ZH, 1-y_pred_ggZH], ["DY-ZH", "gg-ZH"], os.path.join(out_dir, "output_hist.png"), bins=20, xlabel="NN-output", ylabel="Events (normed)")

