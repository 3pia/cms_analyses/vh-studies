import os
import matplotlib.pyplot as plt
from matplotlib.ticker import AutoMinorLocator

larger = 28
large = 26
med = 20

_params = {
    "axes.titlesize": larger,
    "legend.fontsize": med,
    "figure.figsize": (16, 10),
    "axes.labelsize": larger,
    "xtick.labelsize": large,
    "ytick.labelsize": large,
    "figure.titlesize": large,
    "xtick.bottom": True,
    "xtick.direction": "in",
    "ytick.direction": "in",
    "xtick.major.size": 12,
    "ytick.major.size": 12,
    "xtick.minor.size": 8,
    "ytick.minor.size": 8,
    "ytick.left": True,
    }
plt.rcParams.update(_params)


def plot_roc(x, y, path, auc=None):
    fig, ax = plt.subplots(1, 1, figsize=(15, 10))
    ax.plot(x, y, label="AUC: {0:1.2f}".format(auc) if auc else "", color="green")
    ax.set_title("RocCurve", fontsize=24)
    ax.yaxis.set_ticks_position('both')
    ax.xaxis.set_ticks_position('both')
    ax.xaxis.set_minor_locator(AutoMinorLocator())
    ax.set_xlabel("misid. prob. (FP)")
    ax.set_ylabel("id efficency (TP)")
    # ax.set_yscale("log")
    ax.set_xlim(0., 1.)
    ax.set_ylim(9.*1e-4, 1.)
    ax.grid(True, "both", linestyle="dashed")
    ax.legend()
    fig.savefig(os.path.join(path, "roc.png"))

def plot_metric(y_train, y_val, name, path, log=False):
    ax_epochs = range(0,len(y_train))

    fig, ax = plt.subplots(1, 1, figsize=(15, 10))
    ax.plot(ax_epochs, y_train, color='g', label='Training')
    ax.plot(ax_epochs, y_val, color='b', label='Validation')
    ax.set_title('')
    ax.set_xlabel('Epochs')
    ax.set_ylabel('{}'.format(name))
    ax.grid(True, "both", linestyle="dashed")
    ax.legend()
    if log:
        ax.set_yscale("log")
    fig.savefig(os.path.join(path, "metric_{}.png".format(name)))

def plot_hist(datasets, names, fname, **kwargs):
    """
    plot Histogram of n-datasets
    """
    if not isinstance(datasets, list):
        datasets = [datasets]
    if not isinstance(names, list):
        names = [names]

    fig, ax = plt.subplots(1, 1, figsize=(15, 10))

    for dataset, name in zip(datasets, names):
        ax.hist(dataset, bins=kwargs.get("bins", None), density=True, alpha=0.5, label=name)
    ax.set_xlabel(kwargs.get("xlabel", ""))
    ax.set_ylabel(kwargs.get("ylabel", ""))
    ax.legend(loc="upper right")
    ax.grid()
    if kwargs.get("log", False) is True:
        ax.set_yscale("log")
    fig.savefig("{}".format(fname))
