import tensorflow as tf
layers = tf.keras.layers
models = tf.keras.models


# Define TransformerBlock based on https://arxiv.org/pdf/1706.03762.pdf and VISPA transformer example
class TransformerBlock(layers.Layer):
    def __init__(self, embed_dim, num_heads, ff_dim, rate=0.2):
        super(TransformerBlock, self).__init__()
        self.att = layers.MultiHeadAttention(num_heads=num_heads, key_dim=embed_dim // num_heads)
        self.ffn = models.Sequential([layers.Dense(ff_dim, activation="relu"), layers.Dense(embed_dim)])
        self.batchnorm1 = layers.BatchNormalization()
        self.batchnorm2 = layers.BatchNormalization()
        self.dropout1 = layers.Dropout(rate)
        self.dropout2 = layers.Dropout(rate)

    def call(self, input1, input2, training=None):
        attn_output = self.att(input1, input2)
        attn_output = self.dropout1(attn_output, training=training)
        out1 = self.batchnorm1(input1 + attn_output)
        ffn_output = self.ffn(out1)
        ffn_output = self.dropout2(ffn_output, training=training)
        return self.batchnorm2(out1 + ffn_output)

class PositionalEncodingEmbedding(layers.Layer):
    def __init__(self, cls_token=False, **kwargs):
        super(PositionalEncodingEmbedding, self).__init__(**kwargs)
        self.cls_token = cls_token

    def build(self, input_shape):
        self.steps = input_shape[-2]
        self.position_embedding = layers.Embedding(input_dim=self.steps + self.cls_token, output_dim=input_shape[-1])
        if self.cls_token:
            initial_value = tf.zeros((1, 1, input_shape[-1]))
            self.cls_t = tf.Variable(initial_value=initial_value, trainable=True, name="cls")

    def get_config(self):
        config = {'cls_token': self.cls_token}
        base_config = super(PositionalEncodingEmbedding, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))

    def call(self, inputs):
        if self.cls_token:
            n = tf.shape(inputs)[0]
            cls_t = tf.tile(self.cls_t, (n, 1, 1))
            inputs = tf.concat([cls_t, inputs], axis=1)
        positions = tf.range(start=0, limit=self.steps + self.cls_token, delta=1)
        encoded = inputs + self.position_embedding(positions)
        return encoded

def build_transformer():
    #Model definieren
    network_dims = 256
    num_heads = 8
    ff_dim = 32
    n_classes = 6 # WJets in other
    N_blocks = 2
    dropout_rate = 0.2
    # Transformer network:
    # Define inputs
    inp_1 = layers.Input((8, 5))
    inp_2 = layers.Input((1, 3))
    inp_3 = layers.Input((1, 2))
    inp_4 = layers.Input((1, 1))
    inp_5 = layers.Input((1, 1))

    # Embedding
    embedding_vectors = models.Sequential([layers.Dense(network_dims, activation="relu"), layers.Dense(network_dims, activation="relu"), layers.Dense(network_dims, activation="tanh")])
    embedding_three = models.Sequential([layers.Dense(network_dims, activation="relu"), layers.Dense(network_dims, activation="relu"), layers.Dense(network_dims, activation="tanh")])
    embedding_two = models.Sequential([layers.Dense(network_dims, activation="relu"), layers.Dense(network_dims, activation="relu"), layers.Dense(network_dims, activation="tanh")])
    embedding_scalars = models.Sequential([layers.Dense(network_dims, activation="relu"), layers.Dense(network_dims, activation="relu"), layers.Dense(network_dims, activation="tanh")])

    # Define inputs
    inp_1_em = embedding_vectors(inp_1)
    inp_2_em = embedding_three(inp_2)
    inp_3_em = embedding_two(inp_3)
    inp_4_em = embedding_scalars(inp_4)
    inp_5_em = embedding_scalars(inp_5)

    inp_2_em = layers.concatenate([inp_2_em, inp_3_em, inp_4_em, inp_5_em], axis=1)

    # Positional encoding
    x1 = PositionalEncodingEmbedding(cls_token=False)(inp_1_em) 
    x2 = PositionalEncodingEmbedding(cls_token=False)(inp_2_em) 

    # Self attention:
    for i in range(N_blocks):
        print(x1.shape)
        print(x2.shape)
        x1 = TransformerBlock(embed_dim=network_dims, num_heads=num_heads, ff_dim=ff_dim, rate=dropout_rate)(x1, x1)
        x2 = TransformerBlock(embed_dim=network_dims, num_heads=num_heads, ff_dim=ff_dim, rate=dropout_rate)(x2, x2)

    x = x1
    # Cross attention
    for i in range(N_blocks):
        x = TransformerBlock(embed_dim=network_dims, num_heads=num_heads, ff_dim=ff_dim, rate=dropout_rate)(x, x2)

    #Concatenation
    #x = layers.concatenate([x2, x1], axis=1)

    # Combine #Change into class token
    x = layers.GlobalAveragePooling1D()(x) #x1[:, 0]

    # Get final prediction
    x = layers.Dense(network_dims, activation='gelu')(x)
    x = layers.Dense(32, activation='gelu')(x)
    x = layers.Dense(n_classes)(x)
    out = layers.Softmax()(x)

    # Construct network
    lr_schedule = tf.keras.optimizers.schedules.ExponentialDecay(initial_learning_rate=5e-4, decay_steps=10000, decay_rate=0.9)
    model = tf.keras.models.Model({"input_1": inp_1, "input_2": inp_2, "input_3": inp_3, "input_4": inp_4, "input_5": inp_5}, out, name="transformer")
    optimizer = tf.keras.optimizers.Adam(learning_rate=lr_schedule, beta_1=0.9, beta_2=0.999, amsgrad=True)
    model.compile(optimizer, metrics=["accuracy"], loss="CategoricalCrossentropy")
    return model
