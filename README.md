# VH studies

Repository for studies with VH


## Running

To setup the software on VISPA, you can use the 
setup-script `setup.sh`, which sets up environment variables, law-configs and conda. Simply execute:

```
source setup.sh
```

this script also sources `data_setup.sh`, which has to include some datapaths. On VISPA, this includes:

```
export DATA_PATH="/net/scratch/cms/dihiggs/store/zh/Run2_pp_13TeV_2017/CoffeaProcessor/dev1/zh/selection/ClassificationExporter"
```


A training can then be simply run by executing

```
pygpu test_run.py
```